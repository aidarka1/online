<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150430143854 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE report ADD organization_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE report ADD faculty VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE report ADD specialty VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE report ADD specialization VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE report ADD group_number VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE report ADD department_head VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE report ADD company_head VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE report ADD phone_number VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE report ADD path VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F778432C8A3DE FOREIGN KEY (organization_id) REFERENCES organization (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_C42F778432C8A3DE ON report (organization_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE report DROP CONSTRAINT FK_C42F778432C8A3DE');
        $this->addSql('DROP INDEX IDX_C42F778432C8A3DE');
        $this->addSql('ALTER TABLE report DROP organization_id');
        $this->addSql('ALTER TABLE report DROP faculty');
        $this->addSql('ALTER TABLE report DROP specialty');
        $this->addSql('ALTER TABLE report DROP specialization');
        $this->addSql('ALTER TABLE report DROP group_number');
        $this->addSql('ALTER TABLE report DROP department_head');
        $this->addSql('ALTER TABLE report DROP company_head');
        $this->addSql('ALTER TABLE report DROP phone_number');
        $this->addSql('ALTER TABLE report DROP path');
    }
}
