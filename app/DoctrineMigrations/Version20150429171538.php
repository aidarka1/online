<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150429171538 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE report DROP CONSTRAINT fk_c42f7784cb944f1a');
        $this->addSql('DROP INDEX idx_c42f7784cb944f1a');
        $this->addSql('ALTER TABLE report ADD student VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE report DROP student_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE report ADD student_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE report DROP student');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT fk_c42f7784cb944f1a FOREIGN KEY (student_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_c42f7784cb944f1a ON report (student_id)');
    }
}
