<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150430110407 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE grade DROP CONSTRAINT fk_595aae34578d5e91');
        $this->addSql('DROP INDEX idx_595aae34578d5e91');
        $this->addSql('ALTER TABLE grade RENAME COLUMN exam_id TO report_id');
        $this->addSql('ALTER TABLE grade ADD CONSTRAINT FK_595AAE344BD2A4C0 FOREIGN KEY (report_id) REFERENCES report (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_595AAE344BD2A4C0 ON grade (report_id)');
        $this->addSql('ALTER TABLE exam ADD current_report_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE exam ADD CONSTRAINT FK_38BBA6C690F4156C FOREIGN KEY (current_report_id) REFERENCES report (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_38BBA6C690F4156C ON exam (current_report_id)');
        $this->addSql('ALTER TABLE question DROP CONSTRAINT fk_b6f7494e578d5e91');
        $this->addSql('DROP INDEX idx_b6f7494e578d5e91');
        $this->addSql('ALTER TABLE question RENAME COLUMN exam_id TO report_id');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E4BD2A4C0 FOREIGN KEY (report_id) REFERENCES report (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_B6F7494E4BD2A4C0 ON question (report_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE grade DROP CONSTRAINT FK_595AAE344BD2A4C0');
        $this->addSql('DROP INDEX IDX_595AAE344BD2A4C0');
        $this->addSql('ALTER TABLE grade RENAME COLUMN report_id TO exam_id');
        $this->addSql('ALTER TABLE grade ADD CONSTRAINT fk_595aae34578d5e91 FOREIGN KEY (exam_id) REFERENCES grade (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_595aae34578d5e91 ON grade (exam_id)');
        $this->addSql('ALTER TABLE exam DROP CONSTRAINT FK_38BBA6C690F4156C');
        $this->addSql('DROP INDEX UNIQ_38BBA6C690F4156C');
        $this->addSql('ALTER TABLE exam DROP current_report_id');
        $this->addSql('ALTER TABLE question DROP CONSTRAINT FK_B6F7494E4BD2A4C0');
        $this->addSql('DROP INDEX IDX_B6F7494E4BD2A4C0');
        $this->addSql('ALTER TABLE question RENAME COLUMN report_id TO exam_id');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT fk_b6f7494e578d5e91 FOREIGN KEY (exam_id) REFERENCES exam (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_b6f7494e578d5e91 ON question (exam_id)');
    }
}
