Онлайн Защита Диплома
======
Клиент-серверное приложение для проведения экзамена. 
Помимо администратора системы приложение имеет 4 рабочих стола для 4 ролей экзамена: координатор, председатель, эксперт (производственный и кооперативный) и зритель.
Рабочие столы используют сокет соединения с сервером для непрерывной работы с экзаменом. На сервер для сокет сервера стоит Ratchet (http://socketo.me/).
В браузерах используется autobahn (http://autobahn.ws/js/).
Поддержка браузеров - современные браузеры, работающие с сокет соединениями.

* Серверное приложение: php framework - Symfony2
* База данных: postgresql
* Веб сервер: nginx

Требования к серверу
========================
1.  PHP 5.4
2.  zeromq
3.  Opcache
4.  JSON needs to be enabled
5.  ctype needs to be enabled
6.  php.ini needs to have the date.timezone setting
7.  zeromq
8.  PostgreSQL 9.2 + pdo_pgsql
9.  git
10. composer

Дополнительные настройки¶
========================
* You need to have the PHP-XML module installed
* You need to have at least version 2.6.21 of libxml
* PHP tokenizer needs to be enabled
* mbstring functions need to be enabled
* iconv needs to be enabled
* POSIX needs to be enabled (only on *nix)
* Intl needs to be installed with ICU 4+
* APC 3.0.17+ (or another opcode cache needs to be installed)
* php.ini recommended settings
* short_open_tag = Off
* magic_quotes_gpc = Off
* register_globals = Off
* session.auto_start = Off

Разворачивание и конфигурирование проекта
=============================
Клонирование репозитория с исходным кодом в целевую www директорию (Промежуточный репозиторий разместить в /var/local/git/diplom.git , заливаем туда код, рабочий код берет изменения из этого репозитория)
```
cd /var/local/git/diplom.git && git init --bare && cd /var/www/diplom && git clone /var/local/git/diplom.git
```
Установить права на папки кеша и логов
```
HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
```
Установка зависимостей
```
composer update
```
Во время установки зависимостей автоматически будет запущен скрипт конфигурирования, который создаст файл app/config/parameters.yml на основе базового app/config/parameters.yml_dist
Развертывание схемы БД
```
php app/console doctrine:migrations:migrate
```
Создание суперадмина 
```
php app/console fos:user:create admin admin@diplom.dtn admin --super-admin
```
Установка прав на папку хранения пользовательских файлов
```
chmod -R 775 web/uploads/
```
Копирование статики в доступную папку web/bundels
```
php app/console assets:install web --symlink 
```
Обновление кеша окружения "Prod"
```
php app/console cache:clear --env=prod
```

Запуск демона сокетов:
========================
```
php /var/www/diplom/app/console websocket:listen
```

Конфиг nginx типичный для Symfony2 приложений:
========================
```
server {
    server_name online.ec-univer.ru;
    root /var/www/diplom/web;

    location / {
        # try to serve file directly, fallback to app.php
        try_files $uri /app.php$is_args$args;
    }
    # DEV
    # This rule should only be placed on your development environment
    # In production, don't include this and don't deploy app_dev.php or config.php
    location ~ ^/(app_dev|config)\.php(/|$) {
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param HTTPS off;
    }
    # PROD
    location ~ ^/app\.php(/|$) {
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param HTTPS off;
        # Prevents URIs that include the front controller. This will 404:
        # http://domain.tld/app.php/some-path
        # Remove the internal directive to allow URIs like this
        internal;
    }

    error_log /var/log/nginx/diplom.error.log;
    access_log /var/log/nginx/diplom.access.log;
}
```

Обновление проекта
========================
Обновляем проект из промежуточного репозитория
```  
  cd /var/www/diplom && git pull origin master; 
```
Установка зависимостей
```
    composer update
```
Развертывание схемы БД
```
php app/console doctrine:migrations:migrate
```
Копирование статики в доступную папку web/bundels
```
php app/console assets:install web --symlink 
```
Обновление кеша окружения "Prod"
```
php app/console cache:clear --env=prod
```
Создание assetic файлов
```
php app/console assetic:dump --env=prod --no-debug
```