<?php
namespace AppBundle\Handler;

use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    protected $router;
    protected $security;

    public function __construct(Router $router, AuthorizationChecker $security)
    {
        $this->router   = $router;
        $this->security = $security;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        if ($this->security->isGranted('ROLE_ADMIN')){
            $response = new RedirectResponse($this->router->generate('diplom_admin_user_index'));
        }elseif($this->security->isGranted('ROLE_ORGANIZATION_EXPERT')){
            $response = new RedirectResponse($this->router->generate('diplom_expert_index'));
        }elseif($this->security->isGranted('ROLE_CORPORATE_EXPERT')){
            $response = new RedirectResponse($this->router->generate('diplom_expert_index'));
        }elseif($this->security->isGranted('ROLE_VIEWER')){
            $response = new RedirectResponse($this->router->generate('diplom_viewer_index'));
        }elseif($this->security->isGranted('ROLE_COORDINATOR')){
            $response = new RedirectResponse($this->router->generate('diplom_coordinator_index'));
        }elseif($this->security->isGranted('ROLE_CHAIRMAN')){
            $response = new RedirectResponse($this->router->generate('diplom_chairman_index'));
        }
        return $response;
    }
}