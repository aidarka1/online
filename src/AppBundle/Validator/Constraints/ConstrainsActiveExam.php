<?php
/**
 * Created by LLC SIT
 * User: Eduard S. Melnikov
 * Date: 05.05.15
 * Time: 16:31
 */

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ConstrainsActiveExam extends Constraint
{
    public $message = 'В систему уже есть активный экзамен';


    public function validatedBy()
    {
        return 'active_exam_validator';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}