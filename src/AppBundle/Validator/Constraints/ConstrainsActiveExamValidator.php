<?php
/**
 * Created by LLC SIT
 * User: Eduard S. Melnikov
 * Date: 13.02.15
 * Time: 14:46
 */

namespace AppBundle\Validator\Constraints;

use AppBundle\Entity\Exam;
use AppBundle\Enum\ExamStatus;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ConstrainsActiveExamValidator  extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    private $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    public function validate($value, Constraint $constraint)
    {
        /** @var Exam $value */


        $qb = $this->em->createQueryBuilder();
        $qb->select('exam')
            ->from('AppBundle:Exam', 'exam')
            ->where($qb->expr()->in('exam.status', array(ExamStatus::AWAITING_REPORT, ExamStatus::COMPLETED, ExamStatus::PREPARATION, ExamStatus::SPEECH)));

        $currentExam = $qb->getQuery()->getOneOrNullResult();

        if($currentExam instanceof Exam && $currentExam->getId() != $value->getId() && in_array($value->getStatus(), array(ExamStatus::AWAITING_REPORT, ExamStatus::COMPLETED, ExamStatus::PREPARATION, ExamStatus::SPEECH))){
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }

    }

}