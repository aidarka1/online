<?php

namespace AppBundle\Services;

use AppBundle\Entity\Report;
use MBence\OpenTBSBundle\Services\OpenTBS;

class ReportProtocol
{
    private $tbs;
    private $examManager;

    public function __construct(OpenTBS $tbs, ExamManager $exam_manager)
    {
        $this->tbs = $tbs;
        $this->examManager = $exam_manager;
    }

    public function showProtocolByReport(Report $report)
    {
        $this->prepareData($report);
        $this->tbs->Show(OPENTBS_DOWNLOAD, $this->getFileName($report));
    }

    private function prepareData(Report $report)
    {
        $this->initTemplate();
        $this->mergeFields($report);
    }

    private function initTemplate()
    {
        $this->tbs->LoadTemplate(__DIR__ . '/../../../app/Resources/templates/report-protocol.docx');
        $this->tbs->SetOption('charset', 'UTF-8');
    }

    private function mergeFields(Report $report)
    {
        $this->tbs->MergeField('date', date('d.m.Y'));
        $this->tbs->MergeField('report', array(
            'student' => $report->getStudent(),
            'faculty' => $report->getFaculty(),
            'specialty' => $report->getSpecialty(),
            'organization' => $report->getOrganization()->getName(),
            'name' => $report->getName(),
            'group' => $report->getGroupNumber(),
            'head' => $report->getCompanyHead(),
        ));
        $this->tbs->MergeField('grade', $this->examManager->getGradesByReport($report));
        $this->tbs->PlugIn(OPENTBS_CHANGE_PICTURE, '#get_student_photo#', $report->getAbsolutePath(), 'current', 'samewidth');
    }

    private function getFileName(Report $report)
    {
        return sprintf("protocol_%d.docx", $report->getId());
    }
}