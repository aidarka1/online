<?php

namespace AppBundle\Services;

use AppBundle\Application\WampTopicNames;
use AppBundle\Entity\Exam;
use AppBundle\Entity\Grade;
use AppBundle\Entity\User;
use AppBundle\Entity\Question;
use AppBundle\Entity\Report;
use AppBundle\Enum\ExamStatus;
use AppBundle\Enum\GradeAuthorType;
use AppBundle\Enum\GradeType;
use AppBundle\Enum\ReportStatus;
use AppBundle\Exception\ExamException;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\FileBag;

class ExamManager {
    /**
     * @var EntityManager
     */
    private $em;

    private $zeroMqSocket;

    private $wowzaTatneftInnerIp;

    private $wowzaTatneftOuterIp;

    private $screenHandler;

    private $wampLogger;

    public function __construct(EntityManager $em, $wowzaTatneftInnerIp, $wowzaTatneftOuterIp, $zero_mq_port, ScreenHandler $screenHandler, Logger $wamp_logger)
    {
        $this->em = $em;
        $zero_mq = new \ZMQContext();
        $this->zeroMqSocket = $zero_mq->getSocket(\ZMQ::SOCKET_PUSH, 'diplom_zeromq');
        $this->zeroMqSocket->connect('tcp://127.0.0.1:' . $zero_mq_port);
        $this->wowzaTatneftInnerIp = $wowzaTatneftInnerIp;
        $this->wowzaTatneftOuterIp = $wowzaTatneftOuterIp;
        $this->screenHandler = $screenHandler;
        $this->wampLogger = $wamp_logger;
    }

    /**
     * Начать подготовку к экзамену.
     * Примечание: Меняет статус экзамена с "Черновик" на "Подготовка".
     * С экзаменами в статусе "Подготовка" работает координатор
     * @param Exam $exam
     * @throws \AppBundle\Exception\ExamException
     */
    public function startPreparingForExam(Exam $exam)
    {
        if ($this->em->getRepository('AppBundle:Exam')->isActiveExamExist()) {
            throw new ExamException('Уже один из экзаменов проходит в этот момент');
        }

        if ($exam->getStatus() != ExamStatus::DRAFT) {
            throw new ExamException('Экзамен уже начат');
        }

        $exam->setStatus(ExamStatus::PREPARATION);
        $this->em->persist($exam);
        $this->em->flush($exam);

        $this->addMessageToZeroMQ(WampTopicNames::waitExamPreparation(), array(
            'status' => ExamStatus::PREPARATION
        ));
    }

    /**
     * Начать экзамен.
     * @param Exam $exam
     * @throws \AppBundle\Exception\ExamException
     */
    public function startExam(Exam $exam)
    {
        if ($this->em->getRepository('AppBundle:Exam')->isActiveExamExist()) {
            throw new ExamException('Уже один из экзаменов проходит в этот момент');
        }

        if ($exam->getStatus() != ExamStatus::PREPARATION) {
            throw new ExamException('Экзамен уже начат');
        }

        $exam->setStatus(ExamStatus::AWAITING_REPORT);
        $this->em->persist($exam);
        $this->em->flush($exam);

        $this->addMessageToZeroMQ(WampTopicNames::waitExamStart(), array(
            'status' => ExamStatus::AWAITING_REPORT
        ));
    }

    /**
     * Отправить preparation экзамен в черновики
     * @param Exam $exam
     * @throws \AppBundle\Exception\ExamException
     */
    public function stopExamPreparation(Exam $exam)
    {
        if ($exam->getStatus() !== ExamStatus::PREPARATION) {
            throw new ExamException('Экзамен должен быть на подготовке у координатора');
        }

        $exam->setStatus(ExamStatus::DRAFT);
        $this->em->persist($exam);
        $this->em->flush($exam);
    }

    /**
     * Начать доклад
     * @param Report $report
     * @throws \AppBundle\Exception\ExamException
     */
    public function startReport(Report $report)
    {
        $exam = $report->getExam();

        if ($report->getStatus() != ReportStatus::AWAITING) {
            throw new ExamException('Выступление уже начато');
        }

        if ($exam->getStatus() != ExamStatus::AWAITING_REPORT) {
            throw new ExamException('Экзамен должен быть в статусе "в ожидание"');
        }

        $exam->setStatus(ExamStatus::SPEECH);
        $exam->setCurrentReport($report);
        $this->em->persist($exam);

        $report->setStatus(ReportStatus::SPEECH);
        $this->em->persist($report);
        $this->em->flush();

        $active_report = $this->getActiveReport($exam);

        $report_data = array(
            'id' => $active_report->getId(),
            'name' => $active_report->getName(),
            'studentFio' => $active_report->getStudent(),
            'organizationId' => $active_report->getOrganization()->getId(),
            'questions' => array(),
            'status' => $active_report->getStatus()
        );
        $this->addMessageToZeroMQ(WampTopicNames::switchExamReport($exam), array(
            'report' => $report_data,
            'topics' => array(
                WampTopicNames::getReportScreenShots($report) => 'getReportScreenShots',
                WampTopicNames::switchReportStatus($report) => 'switchReportStatus',
                WampTopicNames::getQuestionsByReport($report) => 'getQuestionsByReport',
            )
        ));
        $this->addMessageToZeroMQ(WampTopicNames::switchExamReportForCoordinator($exam), array(
            'report' => $report_data,
            'topics' => array(
                WampTopicNames::switchReportStatus($report) => 'switchReportStatus',
                WampTopicNames::getQuestionsByReport($report) => 'getQuestionsByReport',
                WampTopicNames::getReportGrades($report) => 'getReportGrades',
            )
        ));
    }

    /**
     * Начать обсуждение выступления
     * @param Report $report
     * @throws \AppBundle\Exception\ExamException
     */
    public function startDiscussion(Report $report){
        $exam = $report->getExam();
        if($report->getStatus() != ReportStatus::SPEECH){
            throw new ExamException('Выступление не в статусе "выступление"');
        }

        if($exam->getStatus() != ExamStatus::SPEECH){
            throw new ExamException('Экзамен должен быть в статусе "выступление"');
        }

        $report->setStatus(ReportStatus::DISCUSSION);
        $this->em->persist($report);
        $this->em->flush($report);

        $this->addMessageToZeroMQ(WampTopicNames::switchReportStatus($report), array(
            'status' => ReportStatus::DISCUSSION
        ));
    }

    /**
     * Ввод данных для протокола
     * @param Report $report
     * @throws \AppBundle\Exception\ExamException
     */
    public function startBuildProtocol(Report $report){
        $exam = $report->getExam();

        if($report->getStatus() != ReportStatus::DISCUSSION){
            throw new ExamException('Выступление не в статусе "обсуждение"');
        }

        if($exam->getStatus() != ExamStatus::SPEECH){
            throw new ExamException('Экзамен должен быть в статусе "выступление"');
        }

        $report->setStatus(ReportStatus::BUILD_PROTOCOL);
        $this->em->persist($report);
        $this->em->flush($report);

        $this->addMessageToZeroMQ(WampTopicNames::switchReportStatus($report), array(
            'status' => ReportStatus::BUILD_PROTOCOL
        ));
    }

    /**
     * Закончить доклад
     * @param Report $report
     * @throws \AppBundle\Exception\ExamException
     */
    public function endReport(Report $report){
        $exam = $report->getExam();
        if($report->getStatus() != ReportStatus::BUILD_PROTOCOL){
            throw new ExamException('Выступление не в статусе "формирование протокола"');
        }

        if($exam->getStatus() != ExamStatus::SPEECH){
            throw new ExamException('Экзамен должен быть в статусе "выступление"');
        }

        $report->setStatus(ReportStatus::COMPLETED);
        $this->em->persist($report);
        $this->em->flush($report);

        $exam = $report->getExam();
        $exam->setCurrentReport(null);
        $exam->setStatus(ExamStatus::AWAITING_REPORT);
        $this->em->persist($exam);
        $this->em->flush($exam);

        $this->addMessageToZeroMQ(WampTopicNames::switchReportStatus($report), array(
            'status' => ReportStatus::COMPLETED
        ));
    }

    public function changeReportPosition(Report $report, $position){
        if($report->getStatus() != ReportStatus::AWAITING){
            throw new ExamException('Выступление должно быть в статусе "в ожидании"');
        }

        $report->setPosition($position);
        $this->em->persist($report);
        $this->em->flush();
    }


    /**
     * @param User $user
     * @param Report $report
     * @return bool
     */
    public function wasGraded(User $user, Report $report) {
        if ($this->em->getRepository("AppBundle:Grade")->findOneBy(array(
            "user" => $user,
            "report" => $report,
        ))) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Report $report
     * @return bool
     */
    public function wasAskedBy(User $user, Report $report) {
        if ($this->em->getRepository("AppBundle:Question")->findOneBy(array(
            "user" => $user,
            "report" => $report,
        ))) {
            return true;
        }

        return false;
    }

    /**
     * @param Grade $grade
     * @throws \AppBundle\Exception\ExamException
     */
    public function addGrade(Grade $grade){
        $exam = $grade->getReport()->getExam();

        if ($this->em->getRepository("AppBundle:Grade")->findOneBy(array(
            "user" => $grade->getUser(),
            "report" => $grade->getReport(),
            "type" => $grade->getType()
        ))) {
            throw new ExamException(sprintf('Оценка за "%s" вами уже выставлена', GradeType::getName($grade->getType())));
        }

        if($exam->getStatus() != ExamStatus::SPEECH){
            throw new ExamException('Экзамен должен быть в статусе "выступление"');
        }

        $report = $grade->getReport();

        if( in_array($grade->getAuthorType(), array(GradeAuthorType::CORPORATE_EXPERT, GradeAuthorType::EXPERT)) && ($report->getStatus() != ReportStatus::DISCUSSION) ){
            throw new ExamException('Текущие выступление не имеет статус "Обсуждение"');
        }

        if( ($grade->getAuthorType() == GradeAuthorType::COMMISSION) && ($report->getStatus() != ReportStatus::BUILD_PROTOCOL) ){
            throw new ExamException('Текущие выступление не имеет статус "Формирование протокола"');
        }

        $this->em->persist($grade);
        $this->em->flush($grade);

        $this->addMessageToZeroMQ(WampTopicNames::getReportGrades($report), array(
            'type' => $grade->getType(),
            'author' => $grade->getAuthorType(),
            'grade' => $grade->getValue(),
        ));
    }

    /**
     * Возвращает оценки по отчету, сгруппированные сначала по типу оценки, затем по типу автора
     * Примечание: Если нет оценки по типу, то для данного типа вернет null (чтобы отличить от оценки 0)
     * Сумма (total), считается как сумма средних оценок по типам. (Высчитывается средняя оценка для актуальности,
     * содержания и выступления. А затем 3 оценки суммируются)
     * Пример: ['topicallity' => ['corporate_expert' => 1, 'expert' => null, 'commission' => null]]
     * @param Report $report
     * @return array
     */
    public function getGradesByReport(Report $report)
    {
        $grades = array();
        foreach (GradeType::getChoices() as $key => $value) {
            $grades[$key] = array_map(function() { return null; }, GradeAuthorType::getChoices());
        }

        $data = $this->em->getRepository('AppBundle:Grade')->getGradesAsArrayByReport($report);
        foreach ($data as $row) {
            if (array_key_exists($row['type'], $grades) && array_key_exists($row['authorType'], $grades[$row['type']])) {
                $grades[$row['type']][$row['authorType']] = $row['value'];
            }
        }

        $total = 0;
        foreach ($grades as $type => $type_data) {
            $type_sum = $type_count = 0;
            foreach ($type_data as $author_type => $grade) {
                if ($grade !== null) {
                    $type_sum += $grade;
                    $type_count++;
                }
            }
            if ($type_count > 0 && $type_sum > 0) {
                $total += round($type_sum / $type_count, 1);
            }
        }

        return array_merge(array('total' => $total), $grades);
    }

    /**
     * @param Question $question
     * @throws \AppBundle\Exception\ExamException
     */

    public function addQuestion(Question $question){
        $exam = $question->getReport()->getExam();

        if($exam->getStatus() != ExamStatus::SPEECH){
            throw new ExamException('Экзамен должен быть в статусе "выступление"');
        }

        $report = $question->getReport();

        if ($report->getStatus() != ReportStatus::DISCUSSION ){
            throw new ExamException('Текущие выступление не имеет статус "Обсуждение"');
        }

        if ($this->isQuestionSend($question)) {
            throw new ExamException("Вопрос уже был задан");
        }

        $this->em->persist($question);
        $this->em->flush($question);

        $this->addMessageToZeroMQ(WampTopicNames::getQuestionsByReport($report), array(
            'id' => $question->getId(),
            'author' => $question->getUser()->getUsername(),
            'text' => $question->getText()
        ));
    }

    public function getProtocol(Exam $exam){
        if ($exam != ExamStatus::COMPLETED ){
            throw new ExamException('Экзамен не имеет статус "Обсуждение"');
        }
    }

    /**
     * Возвращает активный экзамен для координатора
     * @return Exam
     * @throws ExamException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getExamForCoordinator()
    {
        $qb = $this->em->getRepository("AppBundle:Exam")->createQueryBuilder('e');

        $exam = $qb
            ->select('e')
            ->where($qb->expr()->notIn('e.status', [ExamStatus::DRAFT, ExamStatus::COMPLETED, ExamStatus::ARCHIVE]))
            ->getQuery()
            ->getOneOrNullResult();

        if (!$exam) {
            throw new ExamException("Активный экзамен не найден");
        }

        return $exam;
    }

    /**
     * Возвращает активный экзамен
     * @return Exam $exam
     * @throws \AppBundle\Exception\ExamException
     */
    public function getActiveExam()
    {
        $qb = $this->em->getRepository("AppBundle:Exam")->createQueryBuilder('e');

        $exam = $qb
            ->select('e')
            ->where($qb->expr()->notIn('e.status', [ExamStatus::DRAFT, ExamStatus::PREPARATION, ExamStatus::COMPLETED,  ExamStatus::ARCHIVE]))
            ->getQuery()
            ->getOneOrNullResult();

        if (!$exam) {
            throw new ExamException("Активный экзамен не найден");
        }

        return $exam;
    }

    /**
     * Возвращает активный доклад экзамена
     * @param Exam $exam
     * @return Report
     * @throws \AppBundle\Exception\ExamException
     */
    public function getActiveReport(Exam $exam)
    {
        $currentReport = $exam->getCurrentReport();
        $isCurrentReportActive = $currentReport &&
            in_array($currentReport->getStatus(), array(ReportStatus::SPEECH,ReportStatus::DISCUSSION, ReportStatus::BUILD_PROTOCOL));

        if (!$isCurrentReportActive) {
            throw new ExamException("Активный доклад не найден");
        }

        return $currentReport;
    }


    /**
     * Возвращает ip видео сервера
     * @return string
     */
    public function getVideoIp(){
        $video_ip = (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) || substr($_SERVER['REMOTE_ADDR'], 0, 3) != '10.')
            ? $this->wowzaTatneftOuterIp
            : $this->wowzaTatneftInnerIp;
        return $video_ip;
    }

    /**
     * Возвращает массив докладов (в ожидании) для сортировки
     * @return array
     */
    public function getReportsForSort()
    {
        $qb = $this->em->getRepository("AppBundle:Exam")->createQueryBuilder('e');

        return $qb
            ->select('r.id, r.name, r.student, r.position')
            ->innerJoin('e.reports', 'r')
            ->where($qb->expr()->notIn('e.status', [ExamStatus::DRAFT, ExamStatus::ARCHIVE]))
            ->andWhere($qb->expr()->eq('r.status', ':status'))
            ->setParameter('status', ReportStatus::AWAITING)
            ->orderBy('r.position', 'asc')
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * Существует ли вопрос для доклада
     * @param Question $question
     * @return bool
     */
    public function isQuestionSend(Question $question)
    {
        $question = $this->em->getRepository("AppBundle:Question")->findOneBy(array('report' => $question->getReport(), 'user' => $question->getUser()));

        return ($question ? true : false);
    }

    /**
     * @param Exam $exam
     */
    public function addNewScreenToZero(Exam $exam) {
        $report = $this->getActiveReport($exam);
        $this->addMessageToZeroMQ(WampTopicNames::getReportScreenShots($report));
    }

    /**
     * Возращает следующий доклад в статусе ожидания
     * @param Exam $exam
     * @param $offset
     * @return Report
     * @throws ExamException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getNextAwaitingReportOrNullResult(Exam $exam, $offset = 0){
        $qb = $this->em->getRepository("AppBundle:Report")->createQueryBuilder('r');

        $report = $qb
            ->where($qb->expr()->eq('r.status', ':status'))
            ->andWhere('r.exam = :exam')
            ->setParameter('status', ReportStatus::AWAITING)
            ->setParameter('exam', $exam->getId())
            ->orderBy('r.position', 'asc')
            ->getQuery()
            ->setMaxResults(1)
            ->setFirstResult($offset)
            ->getOneOrNullResult();

        return $report;
    }

    /**
     * Отправить в архив
     * @param Exam $exam
     * @throws ExamException
     */
    public function sendExamToArchive(Exam $exam)
    {
        if (!$exam->getStatus() == ExamStatus::COMPLETED) {
            throw new ExamException("Нет завершенных экзаменов");
        }

        $exam->setStatus(ExamStatus::ARCHIVE);

        $this->em->persist($exam);
        $this->em->flush();
    }

    /**
     * Завершить экзамен
     * @param Exam $exam
     * @throws ExamException
     */
    public function endExam(Exam $exam)
    {
        if (!in_array($exam->getStatus(), array(ExamStatus::PREPARATION, ExamStatus::AWAITING_REPORT, ExamStatus::COMPLETED))) {
            throw new ExamException("Нет завершенных экзаменов");
        }

        $exam->setStatus(ExamStatus::COMPLETED);
        $this->em->persist($exam);
        $this->em->flush();

        $this->addMessageToZeroMQ(WampTopicNames::waitExamEnd($exam));
    }

    /**
     * Внимание! Сброс экзамена.
     * Устанавливает экзамен и его доклады в исходное состояние. Удаляет вопросы, оценки и скриншоты.
     * Нужен для теста (ленивые менеджеры)
     * @param Exam $exam
     * @throws IOExceptionInterface
     */
    public function resetExam(Exam $exam)
    {
        $exam->setStatus(ExamStatus::DRAFT);
        $exam->setCurrentReport(null);

        foreach ($exam->getReports() as $report) {
            $report->setStatus(ReportStatus::AWAITING);
            foreach ($report->getQuestions() as $question) {
                $this->em->remove($question);
            }
            foreach ($report->getGrades() as $grade) {
                $this->em->remove($grade);
            }
            $this->em->persist($report);
        }

        $file_system = new Filesystem();
        if ($file_system->exists($exam->getUploadScreenRootDir())) {
            $file_system->remove($exam->getUploadScreenRootDir());
        }

        $this->em->persist($exam);
        $this->em->flush();
    }

    /**
     * Добавить сообщение в очередь zeroMQ
     * @param string $topic Наименование топика (см. класс WampTopicNames)
     * @param string|array $data Любое сообщение, которое хотим передать
     */
    private function addMessageToZeroMQ($topic, $data = null)
    {
        $message = array('topic' => $topic);
        if ($data !== null) {
            $message['data'] = $data;
        }

        $json_message = json_encode($message);
        $this->zeroMqSocket->send($json_message);
        $this->wampLogger->addInfo($json_message, array($topic));
    }
}