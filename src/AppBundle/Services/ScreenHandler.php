<?php

namespace AppBundle\Services;


use AppBundle\Entity\Exam;
use AppBundle\Enum\ExamStatus;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\FileBag;

class ScreenHandler {

    public $screenLogger;

    public function __construct(Logger $screenLog)
    {
        $this->screenLogger = $screenLog;
    }

    /**
     * Сохранение скриншота
     * @param FileBag $files
     * @param Exam $exam
     * @return bool|\Symfony\Component\HttpFoundation\File\File
     * @throws \Exception
     */
    public function save(FileBag $files, Exam $exam) {
        if ($exam->getStatus() !== ExamStatus::SPEECH) {
            return false;
        }

        if ($files->count() > 0) {
            /** @var UploadedFile $uploadedFile */
            foreach ($files as $uploadedFile) {

                $filename = date("d.m.Y H:i:s").".".$uploadedFile->getClientOriginalExtension();
                $examDir = $exam->getUploadScreenRootDir();

                if (!is_dir($examDir)) {
                    if(! @mkdir($examDir)){
//                        $mkdirErrorArray = error_get_last();
//                        throw new \Exception('cant create directory ' .$mkdirErrorArray['message'], 1);
                        $this->screenLogger->info("не удалось создать директорию ". $examDir);
                        return false;
                    }
                }

                if ($file = $uploadedFile->move($examDir, $filename)) {
                    $this->screenLogger->info($filename);
                    return $file;
                }

                $this->screenLogger->info("не удалось сохранить скриншот" . $filename);
            }
        }

        return false;
    }

    /**
     * Выдача скриншота с задержкой
     * @param \DateTime $datetime
     * @param Exam $exam
     * @return bool|UploadedFile
     */
    public function getScreen(Exam $exam, \DateTime $datetime)
    {
        if ($exam->getStatus() !== ExamStatus::SPEECH) {
            return false;
        }

        $examDir = $exam->getUploadScreenRootDir();

        $weeds = array('.', '..');
        $files = array_values(array_diff(scandir($examDir), $weeds));

        if (count($files) > 0) {
            //выбираем с учетом задержки
            $datetime->modify($exam->getDelay().' s');

            foreach ($files as $key => $file) {
                $cur = new \DateTime($this->getName($files[$key]));

                if ($cur <= $datetime) {
                    if (!isset($files[$key + 1])) {
                        return new UploadedFile($examDir.'/'.$files[$key], $files[$key]);
                    }

                    $next = new \DateTime($this->getName($files[$key + 1]));

                    if ($datetime < $next) {
                        return new UploadedFile($examDir.'/'.$files[$key], $files[$key]);
                    }
                }
            }
        }

        $this->screenLogger->info("нет скриншотов");

        return false;
    }

    /**
     * Возвращает имя без расширения
     * @param $filename
     * @return mixed
     */
    private function getName($filename)
    {
        return preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
    }
}