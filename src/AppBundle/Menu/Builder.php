<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;

class Builder extends ContainerAware
{
    public function adminMenu(FactoryInterface $factory, array $options)
    {
        /** @var Request $request */
        $request = $this->container->get('request');

        $menu = $factory->createItem('root');
        $menu
            ->addChild('Пользователи', array('route' => 'diplom_admin_user_index'))
                ->setLinkAttribute('class', 'hd_nav-item hd_nav-name')
                ->setExtra('routes', array(
                    array('route' => 'diplom_admin_user_index', 'routeParams' => array('id' => $request->get('id'))),
                    array('route' => 'diplom_admin_user_create', 'routeParams' => array('id' => $request->get('id'))),
                    array('route' => 'diplom_admin_user_edit', 'routeParams' => array('id' => $request->get('id'))),
                    array('route' => 'diplom_admin_user_change_password', 'routeParams' => array('id' => $request->get('id'))),
                ))
            ->addChild('Экзамены', array('route' => 'diplom_admin_exam_index'))
                ->setLinkAttribute('class', 'hd_nav-item hd_nav-name')
                ->setExtra('routes', array(
                    array('route' => 'diplom_admin_exam_index', 'routeParams' => array('id' => $request->get('id'))),
                    array('route' => 'diplom_admin_exam_create', 'routeParams' => array('id' => $request->get('id'))),
                    array('route' => 'diplom_admin_exam_edit', 'routeParams' => array('id' => $request->get('id'))),
                ))
            ->addChild('Организации', array('route' => 'diplom_admin_reference_organization_main'))
                ->setLinkAttribute('class', 'hd_nav-item hd_nav-name')
                ->setExtra('routes', array(
                    array('route' => 'diplom_admin_reference_organization_main', 'routeParams' => array('id' => $request->get('id'))),
                    array('route' => 'diplom_admin_reference_organization_create', 'routeParams' => array('id' => $request->get('id'))),
                    array('route' => 'diplom_admin_reference_organization_edit', 'routeParams' => array('id' => $request->get('id'))),
                ))
        ;

        return $menu;
    }
}