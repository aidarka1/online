<?php

namespace AppBundle\Application;

use Monolog\Logger;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;

/**
 * WAMP приложение для работы с веб сокетами на основе шаблона Publish/Subscribe
 */
class WampProcessor implements WampServerInterface
{
    protected $subscribedTopics = array();
    protected $wampLogger;
    protected $connections;
    protected $connectionsCount;

    public function __construct(Logger $wamp_logger)
    {
        $this->wampLogger = $wamp_logger;
        $this->connections = new \SplObjectStorage();
        $this->connectionsCount = 0;
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->connectionsCount++;
        $this->connections->attach($conn);
        $this->wampLogger->addInfo('Открыто соединение', array(
            'count' => $this->connectionsCount,
            'id' => $conn->resourceId,
            'ip' => $conn->remoteAddress,
        ));
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->connectionsCount--;
        $this->connections->detach($conn);
        $this->wampLogger->addInfo('Закрыто соединение', array(
            'count' => $this->connectionsCount,
            'id' => $conn->resourceId,
        ));
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->wampLogger->addError(sprintf("Ошибка: %s (%s)", $e->getMessage(), $e->getCode()), array(
            'id' => $conn->resourceId,
            'file' => $e->getFile(),
            'line' => $e->getLine(),
        ));
    }

    public function onSubscribe(ConnectionInterface $conn, $topic)
    {
        $this->subscribedTopics[$topic->getId()] = $topic;
        $this->wampLogger->addInfo(sprintf("Установлена подписка на топик %s", $topic->getId()), array(
            'id' => $conn->resourceId,
            'subscribes' => json_encode($this->subscribedTopics),
        ));
    }

    public function onUnSubscribe(ConnectionInterface $conn, $topic)
    {
        if (array_key_exists($topic->getId(), $this->subscribedTopics)) {
            unset($this->subscribedTopics[$topic->getId()]);
        }

        $this->wampLogger->addInfo(sprintf("Разорвана подписка на топик %s", $topic->getId()), array(
            'id' => $conn->resourceId,
            'subscribes' => json_encode($this->subscribedTopics),
        ));
    }

    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
        $this->wampLogger->addWarning('Попытка опубликовать сообщение', array(
            'id' => $conn->resourceId,
            'topic' => $topic->getId(),
            'event' => json_encode($event),
        ));
        // Клиенты не отправляют данные, только получают
        $conn->close();
    }

    public function onCall(ConnectionInterface $conn, $id, $topic, array $params)
    {
        $this->wampLogger->addWarning('Попытка вызвать удаленную процедуру', array(
            'id' => $conn->resourceId,
            'param_id' => $id,
            'topic' => $topic->getId(),
            'params' => json_encode($params),
        ));
        // RPC не используется
        $conn->close();
    }

    public function zeromqMessage($message)
    {
        $data = json_decode($message, true);
        if (!$data || !is_array($data) || !array_key_exists('topic', $data)) {
            return;
        }

        if (!array_key_exists($data['topic'], $this->subscribedTopics)) {
            return;
        }

        $topic = $this->subscribedTopics[$data['topic']];
        $topic->broadcast($data);

        $this->wampLogger->addInfo(sprintf("Разослано сообщение %s", json_encode($data)));
    }
}