<?php

namespace AppBundle\Application;

use AppBundle\Entity\Exam;
use AppBundle\Entity\Report;

/**
 * Возвращает наименования топиков
 * Примечание: Наименования топиков нужны в 2-х местах. На клиентской стороне (для подписки) и при публикации на сервере.
 * Берем наименование топиков для обоих случаев из одного места
 * Class WampTopicNames
 * @package AppBundle\Application
 */
class WampTopicNames
{
    /**
     * Ожидание перевода экзамена из черновика в предварительный статус
     * Примечание: Для страницы координатора
     * @return string
     */
    static public function waitExamPreparation()
    {
        return md5('wait.exam.preparation' . md5(date('mYmm')));
    }

    /**
     * Ожидание начала экзамена.
     * Примечание: На этот топик подписываются пользователи, попавшие в свои личные кабинеты до начала экзамена
     * @return string
     */
    static public function waitExamStart()
    {
        return md5('wait.exam' . md5(date('mYmm')));
    }

    /**
     * Ожидание окончания экзамена.
     * Примечание: На этот топик подписываются все пользователи (эксперты, зрители, координаторы и председатели) при
     * входе в свои кабинеты при начатом экзамене (и в предварительной стадии для координатора)
     * @param Exam $exam
     * @return string
     */
    static public function waitExamEnd(Exam $exam)
    {
        return md5(sprintf("exam%d.wait.end", $exam->getId()));
    }

    /**
     * Топик "Смена активного доклада экзамена"
     * Примечание: Вызывать при активации доклада. При этом клиент отписывается от событий пролого доклада и подписывается на новый доклад
     * @param Exam $exam
     * @return string
     */
    static public function switchExamReport(Exam $exam)
    {
        return md5(sprintf("exam%d.report", $exam->getId()));
    }

    /**
     * Топик "Смена активного доклада экзамена"
     * Примечание: Для страниц экспертов и зрителей нужно выполнять одни действия, для координатора и председателя - другие
     * Поэтому метод switchExamReport использовать для экспертов, switchExamReportForCoordinator для председателей
     * @param Exam $exam
     * @return string
     */
    static public function switchExamReportForCoordinator(Exam $exam)
    {
        return md5(sprintf("exam%d.report.coordinator", $exam->getId()));
    }

    /**
     * Топик "Изменение статуса доклада"
     * Примечание: Изменение статуса с "ожидаение" на "выступление" здесь не фиксируется (отслеживается в методе switchExamReport)
     * @param Report $report
     * @return string
     */
    static public function switchReportStatus(Report $report)
    {
        return md5(sprintf("report%d.status", $report->getId()));
    }

    /**
     * Топик "Подписка на получение новых вопросов по докладу"
     * @param Report $report
     * @return string
     */
    static public function getQuestionsByReport(Report $report)
    {
        return md5(sprintf("report%d.questions", $report->getId()));
    }

    /**
     * Топик "Получение оценок по докладу"
     * @param Report $report
     * @return string
     */
    static public function getReportGrades(Report $report)
    {
        return md5(sprintf("report%d.grades", $report->getId()));
    }

    /**
     * Топик "Получение презентации по докладу"
     * @param Report $report
     * @return string
     */
    static public function getReportScreenShots(Report $report)
    {
        return md5(sprintf("report%d.screens", $report->getId()));
    }
}