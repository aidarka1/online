<?php

namespace AppBundle\Command;

use AppBundle\Application\WampProcessor;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\Wamp\WampServer;
use Ratchet\WebSocket\WsServer;
use React\EventLoop\Factory;
use React\Socket\Server;
use React\ZMQ\Context;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WebSocketListenCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('websocket:listen')
            ->setDescription('Listen for websocket requests (blocks indefinitely)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $loop = Factory::create();
        $application = new WampProcessor($this->getContainer()->get('monolog.logger.wamp_log'));

        // Listen for the web server to make a ZeroMQ push after an ajax request
        $context = new Context($loop);
        $pull = $context->getSocket(\ZMQ::SOCKET_PULL);
        $pull->bind('tcp://127.0.0.1:' . $this->getContainer()->getParameter('zeromq_port'));
        $pull->on('message', array($application, 'zeromqMessage'));

        $web_socket = new Server($loop);
        $web_socket->listen(
            $this->getContainer()->getParameter('websocket_listen_port'),
            $this->getContainer()->getParameter('websocket_listen_interface')
        );

        new IoServer(
            new HttpServer(new WsServer(new WampServer($application))),
            $web_socket
        );

        $loop->run();
    }
}