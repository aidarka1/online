<?php

namespace AppBundle\Command;

use AppBundle\Enum\ExamStatus;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadFileCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('screen:upload')
            ->setDescription('Отправка сообщения о новом скриншоте')
            ->addArgument(
                'exam',
                InputArgument::OPTIONAL
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $screenLogger = $this->getContainer()->get('monolog.logger.screen_log');
        $examId = $input->getArgument('exam');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $exam = $em->getRepository("AppBundle:Exam")->find($examId);

        $examManager = $this->getContainer()->get('diplom_exam_manager');
        sleep($exam->getDelay());
        $examManager->addNewScreenToZero($exam);
    }

}