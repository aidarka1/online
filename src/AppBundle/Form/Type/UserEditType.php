<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, array(
                'label' => 'Ф.И.О',
                'label_attr' => array('class' => 'control-label'),
            ))
            ->add('email', null, array(
                'label' => 'E-mail',
                'label_attr' => array('class' => 'control-label'),
            ));
    }

    public function getName()
    {
        return 'UserEditType';
    }
}