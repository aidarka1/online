<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\DataTransformer\ArrayToStringTransformer;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserCreateType extends AbstractType
{
    /** @var EntityManager */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add($builder->create('roles', 'choice', array(
                'choices' => $this->getRoles(),
                'label' => 'Роль',
            ))->addModelTransformer(new ArrayToStringTransformer()));
    }

    public function getParent()
    {
        return 'fos_user_registration';
    }

    public function getName()
    {
        return 'diplom_user_create';
    }

    private function getRoles()
    {
        $roles = array(
            'ROLE_ADMIN' => 'Админ',
            'ROLE_CHAIRMAN' => 'Председатель',
            'ROLE_COORDINATOR' => 'Координатор',
            'ROLE_ORGANIZATION_EXPERT' => 'Эксперт предприятия',
            'ROLE_VIEWER' => 'Зритель',
        );

        if ($this->em->getRepository('AppBundle:User')->isCorporateExpertExist() === false) {
            $roles['ROLE_CORPORATE_EXPERT'] = 'Корпоративный эксперт';
        }

        return $roles;
    }
}