<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\OrganizationRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Тема диплома',
                'label_attr' => array('class' => 'control-label'),
            ))
            ->add('student', null, array(
                'label' => 'Ф.И.О',
                'label_attr' => array('class' => 'control-label'),
            ))
            ->add('faculty', null, array(
                'label' => 'Факультет',
                'label_attr' => array('class' => 'control-label'),
            ))
            ->add('specialty', null, array(
                'label' => 'Специальность',
                'label_attr' => array('class' => 'control-label'),
            ))
            ->add('specialization', null, array(
                'label' => 'Специализация',
                'label_attr' => array('class' => 'control-label'),
            ))
            ->add('groupNumber', null, array(
                'label' => 'Номер группы',
                'label_attr' => array('class' => 'control-label'),
            ))
            ->add('departmentHead', null, array(
                'label' => 'Руководитель от кафедры',
                'label_attr' => array('class' => 'control-label'),
            ))
            ->add('companyHead', null, array(
                'label' => 'Руководитель от предприятия',
                'label_attr' => array('class' => 'control-label'),
            ))
            ->add('averageGrade', null, array(
                'label' => 'Средний бал',
                'label_attr' => array('class' => 'control-label'),
            ))
            ->add('phoneNumber', null, array(
                'label' => 'Номер телефона',
                'label_attr' => array('class' => 'control-label'),
            ))
            ->add('file', 'file', array(
                'image_path' => 'webPath',
                'label_attr' => array('class' => 'control-label'),
            ))
            ->add('organization', 'entity', array(
                'class' => 'AppBundle\Entity\Organization',
                'placeholder' => '',
                'query_builder' => function(OrganizationRepository $or) {
                    return $or->getVisibleOrganizationsQueryBuilder();
                },
                'label' => 'Место прохождения практики',
                'label_attr' => array('class' => 'control-label'),
            ))
        ;
    }

    public function getName()
    {
        return 'ReportType';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Report',
        ));
    }
}