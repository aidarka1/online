<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class GradeExpertType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gradeTopicality', 'number', array(
                'mapped' => false,
                'required' => true,
                'error_bubbling' => true,
                'constraints' => array(
                    new NotBlank(),
                    new Range(['min' => 0, 'max' => 2])
                )
            ))
            ->add('gradeContent', 'number', array(
                'mapped' => false,
                'error_bubbling' => true,
                'constraints' => array(
                    new NotBlank(),
                    new Range(['min' => 0, 'max' => 2])
                )
            ))
            ->add('gradeSkill', 'number', array(
                'mapped' => false,
                'error_bubbling' => true,
                'constraints' => array(
                    new NotBlank(),
                    new Range(['min' => 0, 'max' => 1])
                )
            ))
            ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'ExamExpertType';
    }

} 