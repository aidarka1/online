<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OrganizationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array(
                'label' => 'Название'
            ))
            ->add('expert', null, array(
                'label' => 'Эксперт',
                'query_builder' => function (UserRepository $ur) use ($options) {
                    return $ur->findAvailableOrganizationExpertsQueryBuilder($options['data']);
                }
            ));
    }

    public function getName()
    {
        return 'OrganizationType';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Organization',
        ));
    }
} 