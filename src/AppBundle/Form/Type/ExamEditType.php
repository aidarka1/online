<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Valid;

class ExamEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array('label' => 'Экзамен'))
            ->add('chairman', 'entity', array(
                'label' => 'Председатель',
                'class' => 'AppBundle\Entity\User',
                'query_builder' => function (UserRepository $ur) {
                    return $ur->findChairmansOrderedByName(true);
                }
            ))
            ->add('coordinator', 'entity', array(
                'label' => 'Координатор',
                'class' => 'AppBundle\Entity\User',
                'query_builder' => function (UserRepository $ur) {
                    return $ur->findCoordinatorsOrderedByName(true);
                }
            ))
            ->add('delay', null, array('label' => 'Задержка презентации'))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Exam',
            'cascade_validation' => true,
        ));
    }

    public function getName()
    {
        return 'ExamEditType';
    }
}