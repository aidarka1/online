<?php

namespace AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class ArrayToStringTransformer implements DataTransformerInterface
{
    public function transform($array)
    {
        if ($array === null || !is_array($array)) {
            return '';
        }

        return count($array) ? $array[0] : '';
    }

    public function reverseTransform($string)
    {
        return array($string);
    }
}