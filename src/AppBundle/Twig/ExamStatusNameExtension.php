<?php
/**
 * Created by LLC SIT
 * User: Eduard S. Melnikov
 * Date: 14.05.15
 * Time: 15:10
 */
namespace AppBundle\Twig;

use AppBundle\Enum\ExamStatus;

class ExamStatusNameExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('examStatus', array($this, 'priceFilter')),
        );
    }

    public function priceFilter($status)
    {
        return ExamStatus::getName($status);
    }

    public function getName()
    {
        return 'exam_status_extension';
    }
}