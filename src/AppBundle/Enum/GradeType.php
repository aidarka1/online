<?

namespace AppBundle\Enum;

class GradeType{
    const TOPICALITY = 'topicality';
    const CONTENT = 'content';
    const SKILL = 'skill';

    public static function getChoices(){
        return array(
            self::TOPICALITY => 'Актуальность и новизна',
            self::CONTENT => 'Содержание презентации и доклада',
            self::SKILL => 'Мастерство выступления'
        );
    }

    public static function getName($code){
        return self::getChoices()[$code];
    }
}