<?

namespace AppBundle\Enum;

class ExamStatus{
    const DRAFT = 'draft';
    const PREPARATION = 'preparation';
    const AWAITING_REPORT = 'awaiting';
    const SPEECH = 'speech';
    const COMPLETED = 'completed';
    const ARCHIVE = 'archive';

    public static function getChoices(){
        return array(
            self::DRAFT => 'черновик',
            self::PREPARATION => 'подготовка',
            self::AWAITING_REPORT => 'в ожидании доклада',
            self::SPEECH => 'выступление',
            self::COMPLETED => 'завершен',
            self::ARCHIVE => 'архив',
        );
    }

    public static function getActiveStatuses(){
        return array(
            self::PREPARATION => 'подготовка',
            self::AWAITING_REPORT => 'в ожидании доклада',
            self::SPEECH => 'выступление',
        );
    }

    public static function getName($code){
        return self::getChoices()[$code];
    }
}