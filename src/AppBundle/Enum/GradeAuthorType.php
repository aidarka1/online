<?

namespace AppBundle\Enum;

class GradeAuthorType
{
    const CORPORATE_EXPERT = 'corporate_expert';
    const EXPERT = 'expert';
    const COMMISSION = 'commission';

    public static function getChoices()
    {
        return array(
            self::CORPORATE_EXPERT => 'Корпоративный Эксперт',
            self::EXPERT => 'Эксперт',
            self::COMMISSION => 'Экзаменационная комиссия',
        );
    }

    public static function getName($code)
    {
        return self::getChoices()[$code];
    }
}