<?

namespace AppBundle\Enum;

class ReportStatus{

    const AWAITING = 'awaiting';
    const SPEECH = 'speech';
    const DISCUSSION = 'discussion';
    const BUILD_PROTOCOL = 'build_protocol';
    const COMPLETED = 'completed';

    public static function getChoices(){
        return array(
            self::AWAITING => 'в ожидании',
            self::SPEECH => 'выступление',
            self::DISCUSSION => 'обсуждение',
            self::BUILD_PROTOCOL => 'формирование протокола',
            self::COMPLETED => 'завершен',
        );
    }

    public static function getName($code){
        return self::getChoices()[$code];
    }
}