<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $router = $this->get('router');
        $security = $this->get('security.authorization_checker');

        if ($security->isGranted('ROLE_ADMIN')) {
            $url = $router->generate('diplom_admin_user_index');
        } elseif ($security->isGranted('ROLE_EXPERT')) {
            $url = $router->generate('diplom_expert_index');
        } elseif ($security->isGranted('ROLE_VIEWER')) {
            $url = $router->generate('diplom_viewer_index');
        } elseif ($security->isGranted('ROLE_COORDINATOR')) {
            $url = $router->generate('diplom_coordinator_index');
        } elseif ($security->isGranted('ROLE_CHAIRMAN')) {
            $url = $router->generate('diplom_chairman_index');
        } else {
            $url = $router->generate('fos_user_security_login');
        }

        return $this->redirect($url);
    }

    public function browsersAction()
    {
        return $this->render(':AppBundle/default:browsers.html.twig', array(
            'websocket_port' => $this->container->getParameter('websocket_listen_port'),
        ));
    }
}
