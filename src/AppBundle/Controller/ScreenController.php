<?php

namespace AppBundle\Controller;

use AppBundle\Enum\ExamStatus;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\FileBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\ProcessBuilder;

class ScreenController extends Controller{

    /**
     * Загрузка изображения (с клиентского приложения screenSender)
     * @param Request $request
     * @return Response
     * @throws \RuntimeException
     */
    public function uploadAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $screenHandler = $this->get("diplom_screen.handler");

        if ($examId = $request->get('password')) {
            $exam = $em->getRepository("AppBundle:Exam")->find($examId);

            if (!$exam) {
                return new Response('Экзамен не найден');
            }

            $appDir = $this->get('kernel')->getRootDir();
            $bag = $request->files;

            /** @var UploadedFile $file */
            if ($file = $bag->get('file')) {
                if ($file = $screenHandler->save($request->files, $exam)) {
                    $command = sprintf('cd %s && php console screen:upload %s', $appDir, $exam->getId());
                    $process = new Process($command);
                    $process->run();

                    // executes after the command finishes
                    if (!$process->isSuccessful()) {
                        throw new \RuntimeException($process->getErrorOutput());
                    }

                    return new Response("файл отправлен", 201);
                }
            }
        }

        return new Response("Нет файла", 500);
    }

    /**
     * Тестовый метод для проверки
     */
    public function viewAction()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $exam = $em->getRepository("AppBundle:Exam")->findOneBy(array('status' => ExamStatus::SPEECH));
        $screenHandler = $this->get("diplom_screen.handler");
        $datetime = new \DateTime();
        $file = $screenHandler->getScreen($exam, $datetime);
        var_dump($file); die;
    }
}