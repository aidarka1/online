<?php
/**
 * Created by PhpStorm.
 * User: Aydar.Hannanov
 * Date: 06.05.15
 * Time: 11:23
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Exam;
use AppBundle\Entity\Grade;
use AppBundle\Entity\Question;
use AppBundle\Entity\Report;
use AppBundle\Entity\User;
use AppBundle\Enum\GradeAuthorType;
use AppBundle\Enum\GradeType;
use AppBundle\Exception\ExamException;
use AppBundle\Form\Type\GradeExpertType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AjaxController extends Controller
{
    /**
     * Начать экзамен (координатор начинает экзамен)
     * @param Request $request
     * @return JsonResponse
     */
    public function startExamAction($id, Request $request)
    {
        $examManager = $this->get('diplom_exam_manager');
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        if (!$exam = $em->getRepository("AppBundle:Exam")->find($id)) {
            $this->getErrorResponse("экзамен не найден");
        }

        try {
            $examManager->startExam($exam);
        } catch (ExamException $e) {
            return $this->getErrorResponse($e->getMessage());
        }

        return $this->getSuccessResponse();
    }

    /**
     * Начать доклад (координатор начинает доклад)
     * @param Request $request
     * @return JsonResponse
     */
    public function startReportAction($id, Request $request)
    {
        $examManager = $this->get('diplom_exam_manager');
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        if (!$exam = $em->getRepository("AppBundle:Exam")->find($id)) {
            $this->getErrorResponse("экзамен не найден");
        }

        try {
            $report = $examManager->getNextAwaitingReportOrNullResult($exam);
            $examManager->startReport($report);
        } catch (ExamException $e) {
            return $this->getErrorResponse($e->getMessage());
        }

        return $this->getSuccessResponse();
    }

    /**
     * перейти к обсуждению (координатор нажимает)
     * @param Request $request
     * @return JsonResponse
     */
    public function reportToDiscussionAction($id, Request $request)
    {
        $examManager = $this->get('diplom_exam_manager');
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        if (!$exam = $em->getRepository("AppBundle:Exam")->find($id)) {
            $this->getErrorResponse("экзамен не найден");
        }

        try {
            $examManager->startDiscussion($examManager->getActiveReport($exam));
        } catch (ExamException $e) {
            return $this->getErrorResponse($e->getMessage());
        }

        return $this->getSuccessResponse();
    }

    /**
     * перейти к формированию протокола
     * @param Request $request
     * @return JsonResponse
     */
    public function reportToProtocolAction($id, Request $request)
    {
        $examManager = $this->get('diplom_exam_manager');
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        if (!$exam = $em->getRepository("AppBundle:Exam")->find($id)) {
            $this->getErrorResponse("экзамен не найден");
        }

        try {
            $examManager->startBuildProtocol($examManager->getActiveReport($exam));
        } catch (ExamException $e) {
            return $this->getErrorResponse($e->getMessage());
        }

        return $this->getSuccessResponse();
    }

    /**
     * Сохранить оценки экзаменационной комиссии
     * @param Exam $exam
     * @param Request $request
     * @return JsonResponse
     */
    public function saveCommissionGradesAction(Exam $exam, Request $request)
    {
        $exam_manager = $this->get('diplom_exam_manager');

        $form = $this->createForm(new GradeExpertType());
        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                $arrTypeValue[GradeType::TOPICALITY] = $form->get('gradeTopicality')->getData();
                $arrTypeValue[GradeType::CONTENT] = $form->get('gradeContent')->getData();
                $arrTypeValue[GradeType::SKILL] = $form->get('gradeSkill')->getData();

                foreach ($arrTypeValue as $type => $value) {
                    $grade = new Grade();
                    $grade->setUser($this->getUser());
                    $grade->setAuthorType(GradeAuthorType::COMMISSION);
                    $grade->setReport($exam->getCurrentReport());
                    $grade->setType($type);
                    $grade->setValue($value);

                    $exam_manager->addGrade($grade);
                }
            } catch (ExamException $e) {
                return $this->getErrorResponse($e->getMessage());
            }
        } else {
            $errors = array();
            /** @var FormError $error */
            foreach ($form->getErrors() as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }

            return $this->getCustomResponse(array('status' => 'ok', 'errors' => $errors));
        }

        return $this->getSuccessResponse();
    }

    /**
     * Поставить оценки экзаменационной комиссии
     * Завершить доклад
     * @param Exam $exam
     * @return JsonResponse
     */
    public function endReportAction(Exam $exam)
    {
        $exam_manager = $this->get('diplom_exam_manager');

        try {
            $exam_manager->endReport($exam->getCurrentReport($exam));
        } catch (ExamException $e) {
            return $this->getErrorResponse($e->getMessage());
        }

        $next_report = $exam_manager->getNextAwaitingReportOrNullResult($exam);
        $next_next_report = $exam_manager->getNextAwaitingReportOrNullResult($exam, 1);

        $exam_params = array(
            'nextStudentFio' => $next_report ? $next_report->getStudent() : '',
            'nextTheme' => $next_report ? $next_report->getName() : '',
            'nextNextStudentFio' => $next_next_report ? $next_next_report->getStudent() : '',
        );

        return $this->getSuccessResponse($exam_params);
    }

    /**
     * Обновление сортировки
     * @param $id
     * @param Request $request
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return JsonResponse;
     */
    public function changeReportPositionAction($id, Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var Report $report */
        $report = $em->getRepository("AppBundle:Report")->find($id);

        if (!$report) {
            $this->getErrorResponse("отчет не найден");
        }

        $position = $request->request->get("position");
        $report->setPosition($position);

        $em->persist($report);
        $em->flush();

        $exam = $report->getExam();
        $nextReport = $this->get('diplom_exam_manager')->getNextAwaitingReportOrNullResult($exam);
        $nextNextReport = $this->get('diplom_exam_manager')->getNextAwaitingReportOrNullResult($exam, 1);

        return $this->getSuccessResponse(array(
            'fio' => $nextReport ? $nextReport->getStudent() : '',
            'theme' => $nextReport ? $nextReport->getName() : '',
            'nextStudentFio' => $nextNextReport ? $nextNextReport->getStudent() : ''
        ));
    }

    /**
     * Добавление вопроса
     * @param Request $request
     * @return JsonResponse
     */
    public function addQuestionAction(Request $request)
    {
        if (!$text = $request->get('text')) {
            return $this->getErrorResponse("Не указан текст вопроса");
        }

        $examManager = $this->get('diplom_exam_manager');

        try {
            $exam = $examManager->getActiveExam();
            $report = $examManager->getActiveReport($exam);

            $question = new Question();
            $question->setText($text);
            $question->setReport($report);
            $question->setUser($this->getUser());
            $examManager->addQuestion($question);

        } catch (ExamException $e) {
            return $this->getErrorResponse($e->getMessage());
        }

        return $this->getSuccessResponse($question->getText());
    }

    /**
     * @return JsonResponse
     */
    public function getLastScreenAction()
    {
        $examManager = $this->get('diplom_exam_manager');
        $exam = $examManager->getActiveExam();
        $screenHandler = $this->get("diplom_screen.handler");

        try {
            if (!$file = $screenHandler->getScreen($exam, new \DateTime())) {
                return $this->getSuccessResponse("", "Файл не найден");
            }
        } catch (FileNotFoundException $e) {
            return $this->getSuccessResponse("", "Файл не найден");
        }

        return $this->getSuccessResponse($exam->getUploadScreenDir().'/'.$file->getClientOriginalName());
    }

    /**
     * Активный доклад
     * @param Exam $exam
     * @return JsonResponse
     */
    public function getActiveReport(Exam $exam)
    {
        $report = $this->get('diplom_exam_manager')->getActiveReport($exam);

        return $this->getCustomResponse($report);
    }

    /**
     * Список докладов в ожидании
     * @return JsonResponse
     */
    public function getAwaitingReportsAction()
    {
        $reports = $this->get('diplom_exam_manager')->getReportsForSort();

        return $this->getSuccessResponse(array('reports' => $reports));
    }

    public function addGradesAction(Report $report, Request $request)
    {
        $gradeForm = $this->createForm(new GradeExpertType());
        $gradeForm->handleRequest($request);

        if ($gradeForm->isValid()) {
            $examManager = $this->get('diplom_exam_manager');
            /** @var User $user */
            $user = $this->getUser();

            if (!$user->hasRole('ROLE_CORPORATE_EXPERT') && !$user->hasRole('ROLE_ORGANIZATION_EXPERT')) {
                return $this->getErrorResponse("Оценки могут выставлять только эксперты");
            }

            if ($user->hasRole('ROLE_ORGANIZATION_EXPERT')) {
                $gradeAuthorType = GradeAuthorType::EXPERT;
            } elseif ($user->hasRole('ROLE_CORPORATE_EXPERT')) {
                $gradeAuthorType = GradeAuthorType::CORPORATE_EXPERT;
            }

            try {
                //оценка за актуальность
                $gradeTopicality = new Grade();
                $gradeTopicality->setUser($user);
                $gradeTopicality->setAuthorType($gradeAuthorType);
                $gradeTopicality->setReport($report);
                $gradeTopicality->setType(GradeType::TOPICALITY);
                $gradeTopicality->setValue($gradeForm->get('gradeTopicality')->getData());
                $examManager->addGrade($gradeTopicality);
                //оценка за содержание
                $gradeContent = new Grade();
                $gradeContent->setUser($user);
                $gradeContent->setReport($report);
                $gradeContent->setType(GradeType::CONTENT);
                $gradeContent->setAuthorType($gradeAuthorType);
                $gradeContent->setValue($gradeForm->get('gradeContent')->getData());
                $examManager->addGrade($gradeContent);
                //оценка за мастерство
                $gradeSkill = new Grade();
                $gradeSkill->setUser($user);
                $gradeSkill->setAuthorType($gradeAuthorType);
                $gradeSkill->setReport($report);
                $gradeSkill->setType(GradeType::SKILL);
                $gradeSkill->setValue($gradeForm->get('gradeSkill')->getData());
                $examManager->addGrade($gradeSkill);
            } catch (ExamException $e) {
                return $this->getErrorResponse($e->getMessage());
            }
        } else {
            $errors = [];
            /** @var FormError $error */
            foreach ($gradeForm->getErrors() as $error) {
                $errors[$error->getOrigin()->getName()] = $error->getMessage();
            }

            return $this->getCustomResponse(array('status' => 'ok', 'errors' => $errors));
        }

        return $this->getSuccessResponse();
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    protected function getErrorResponse($message)
    {
        $response = new JsonResponse();
        $response->setContent(json_encode(array(
            'status'    => 'error',
            'message'   => $message
        )));

        return $response;
    }

    /**
     * @param null|array $data
     * @param null|array $message
     * @return JsonResponse
     */
    protected function getSuccessResponse($data = null, $message = null)
    {
        $response = new JsonResponse();

        if (!is_null($data)) {
            $ret['data'] = $data;
        }

        if ($message) {
            $ret['message'] = $message;
        }

        $ret['status'] = 'ok';

        $response->setContent(json_encode($ret));

        return $response;
    }

    /**
     * Особый тип ошибки
     * На клиенте необходимо либо обновить страницу (и произойдет редирект на страницу аутентификации)
     * либо сами сразу переходим туда
     * @return JsonResponse
     */
    protected function getNeedLoginErrorResponse()
    {
        return $this->getErrorResponse('login');
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    protected function getCustomResponse($message)
    {
        $response = new JsonResponse();
        $response->setContent(json_encode($message));

        return $response;
    }

    /**
     * Завершить экзамен
     * @param Request $request
     * @return JsonResponse
     */
    public function endExamAction($id, Request $request)
    {
        $examManager = $this->get('diplom_exam_manager');

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        if (!$exam = $em->getRepository("AppBundle:Exam")->find($id)) {
            $this->getErrorResponse("экзамен не найден");
        }

        try {
            $examManager->endExam($exam);
        } catch (ExamException $e) {
            return $this->getErrorResponse($e->getMessage());
        }

        return $this->getSuccessResponse();
    }
} 