<?php

namespace AppBundle\Controller;

use AppBundle\Application\WampTopicNames;
use AppBundle\Entity\User;
use AppBundle\Exception\ExamException;
use AppBundle\Form\Type\GradeExpertType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ExpertController extends Controller
{
    public function indexAction($role)
    {
        try {
            $exam = $this->get('diplom_exam_manager')->getActiveExam();
        } catch (ExamException $e) {
            return $this->render(':AppBundle:index_wait.html.twig', array(
                'topics' => json_encode(array(WampTopicNames::waitExamStart() => 'waitExamStart')),
                'websocket_port' => $this->container->getParameter('websocket_listen_port'),
            ));
        }

        $exam_params = array(
            'id' => $exam->getId(),
            'name' => $exam->getName(),
            'status' => $exam->getStatus(),
            'videoPath' => 'live',
            'videoStream' => 'diplom.stream',
            'videoPort' => $this->container->getParameter('wowza_tatneft_port'),
            'videoIp' => $this->get('diplom_exam_manager')->getVideoIp(),
            'videoSwfPath' => $this->get('templating.helper.assets')->getUrl('bundles/app/js/jplayer/jquery.jplayer.swf'),
            'videoPoster' => $this->get('templating.helper.assets')->getUrl('bundles/app/img/video_preview.jpg'),
            'presentationPoster' => $this->get('templating.helper.assets')->getUrl('bundles/app/img/presentation_preview.jpg'),
        );

        // Постоянная подписка. От этих топиков не отписываемся при смене доклада
        $invariable_topics = array(
            WampTopicNames::switchExamReport($exam) => 'switchExamReport',
            WampTopicNames::waitExamEnd($exam) => 'waitExamEnd',
        );

        try {
            $report = $this->get('diplom_exam_manager')->getActiveReport($exam);
            $questions = array();
            foreach ($report->getQuestions() as $question) {
                $questions[] = array(
                    'id' => $question->getId(),
                    'text' => $question->getText(),
                    'author' => $question->getUser()->getUsername(),
                );
            }
            $report_params = array(
                'id' => $report->getId(),
                'name' => $report->getName(),
                'studentFio' => $report->getStudent(),
                'status' => $report->getStatus(),
                'questions' => $questions,
                'organizationId' => $report->getOrganization()->getId()
            );
            $topics = array(
                WampTopicNames::getQuestionsByReport($report) => 'getQuestionsByReport',
                WampTopicNames::switchReportStatus($report) => 'switchReportStatus',
                WampTopicNames::getReportScreenShots($report) => 'getReportScreenShots',
            );
        } catch (ExamException $e) {
            $report_params = null;
            $topics = array();
        }

        $gradeForm = $this->createForm(new GradeExpertType(), null);

        $organization_id = null;
        $isCorporateExpert = false;
        /** @var User $user */
        $user = $this->getUser();

        $wasGraded = false;
        if (isset($report)) {
            $wasGraded = $this->get('diplom_exam_manager')->wasGraded($user, $report);
        }

        $wasAsked = false;
        if (isset($report)) {
            $wasAsked = $this->get('diplom_exam_manager')->wasAskedBy($user, $report);
        }

        if ($user->hasRole('ROLE_ORGANIZATION_EXPERT') && $user->getExpertOrganization()) {
            $organization_id = $user->getExpertOrganization()->getId();
        }

        if ($user->hasRole('ROLE_CORPORATE_EXPERT')) {
            $isCorporateExpert = true;
        }

        return $this->render(':AppBundle/expert:index.html.twig', array(
            'role' => $role,
            'invariable_topics' => json_encode($invariable_topics),
            'topics' => json_encode($topics),
            'examParams' => json_encode($exam_params),
            'reportParams' => json_encode($report_params),
            'gradeForm' => $gradeForm->createView(),
            'websocket_port' => $this->container->getParameter('websocket_listen_port'),
            'organizationId' => $organization_id,
            'isCorporateExpert' => $isCorporateExpert,
            'wasGraded' => $wasGraded,
            'wasAsked' => $wasAsked,
        ));
    }
}