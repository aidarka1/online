<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\User;
use AppBundle\Form\Type\UserEditType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    public function indexAction(Request $request)
    {
        $users = $this->getDoctrine()->getManager()->getRepository('AppBundle:User')->findAvailableUsersOrderedByName();
        $pagination = $this->get('knp_paginator')->paginate($users, $request->query->get('page', 1));

        return $this->render(':AppBundle/admin/user:index.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    public function createAction(Request $request)
    {
        $user_manager = $this->get('fos_user.user_manager');
        $user = $user_manager->createUser();
        $user->setEnabled(true);

        $form_factory = $this->get('fos_user.registration.form.factory');
        $form = $form_factory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $user_manager->updateUser($user);
            return $this->redirectToRoute('diplom_admin_user_index');
        }

        return $this->render(':AppBundle/admin/user:create.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction(Request $request, User $user)
    {
        $form = $this->createForm(new UserEditType(), $user, array('validation_groups' => array('Edit')));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('fos_user.user_manager')->updateUser($user);
            return $this->redirectToRoute('diplom_admin_user_index');
        }

        return $this->render(':AppBundle/admin/user:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function changePasswordAction(Request $request, User $user)
    {
        $user_manager = $this->get('fos_user.user_manager');

        $form_factory = $this->get('fos_user.resetting.form.factory');
        $form = $form_factory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $user_manager->updateUser($user);
            return $this->redirectToRoute('diplom_admin_user_index');
        }

        return $this->render(':AppBundle/admin/user:change_password.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function lockAction(User $user)
    {
        if (!$user->isLocked() && !$user->isSuperAdmin()) {
            $user->setLocked(true);
            $this->get('fos_user.user_manager')->updateUser($user);
        }

        return $this->redirectToRoute('diplom_admin_user_index');
    }

    public function unLockAction(User $user)
    {
        if ($user->isLocked()) {
            if ($user->hasRole('ROLE_CORPORATE_EXPERT') && $this->getDoctrine()->getManager()->getRepository('AppBundle:User')->isCorporateExpertExist()) {
                $this->addFlash('error', 'Пользователь с ролью "Корпоративный эксперт" может быть только один. Необходимо снять роль с одного из пользователей (активного либо восстанавливаемого)');
                return $this->redirectToRoute('diplom_admin_user_index');
            }
            $user->setLocked(false);
            $this->get('fos_user.user_manager')->updateUser($user);
        }

        return $this->redirectToRoute('diplom_admin_user_index');
    }
}