<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Organization;
use AppBundle\Form\Type\OrganizationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class OrganizationController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $organizations = $em->getRepository('AppBundle:Organization')->getVisibleOrganizations();
        return $this->render(':AppBundle/admin/organization:index.html.twig', array(
            'organizations' => $organizations,
        ));
    }

    public function createAction(Request $request)
    {
        $organization = new Organization();

        $form = $this->createForm(new OrganizationType(), $organization);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($organization);
            $em->flush();

            return $this->redirectToRoute('diplom_admin_reference_organization_main');
        }

        return $this->render(':AppBundle/admin/organization:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request, Organization $organization)
    {
        if ($organization->getVisible() === false) {
            return $this->redirectToRoute('diplom_admin_reference_organization_main');
        }

        $form = $this->createForm(new OrganizationType(), $organization);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($organization);
            $em->flush();

            return $this->redirectToRoute('diplom_admin_reference_organization_main');
        }

        return $this->render(':AppBundle/admin/organization:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function removeAction(Organization $organization)
    {
        if ($organization->getVisible() === false) {
            return $this->redirectToRoute('diplom_admin_reference_organization_main');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($organization);
        $em->flush();

        return $this->redirectToRoute('diplom_admin_reference_organization_main');
    }

    public function changePositionAction(Organization $organization)
    {
        if ($organization->getVisible() === false) {
            return $this->redirectToRoute('diplom_admin_reference_organization_main');
        }

        // TODO не нашел как менялось сортировка раньше, т.к. что пока и сейчас не будет
        return $this->redirectToRoute('diplom_admin_reference_organization_main');
    }
}
