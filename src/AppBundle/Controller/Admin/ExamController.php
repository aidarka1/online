<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Exam;
use AppBundle\Exception\ExamException;
use AppBundle\Form\Type\ExamEditType;
use AppBundle\Security\Authorization\Voter\ExamVoter;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\HttpFoundation\Request;

class ExamController extends Controller
{
    public function indexAction()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var Exam[] $exams */
        $exams = $em->getRepository("AppBundle:Exam")->findAll();
        $activeExam = $em->getRepository('AppBundle:Exam')->getExamNotInPreparationAndDraftAndArchive();

        return $this->render(':AppBundle/admin/exam:index.html.twig', array(
            'exams' => $exams,
            'activeExam' => $activeExam
        ));
    }

    public function createAction(Request $request)
    {
        $exam = new Exam();
        $form = $this->createForm(new ExamEditType(), $exam);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $em->persist($exam);
            $em->flush();

            return $this->redirectToRoute("diplom_admin_exam_index");
        }

        return $this->render(':AppBundle/admin/exam:create.html.twig', array('form' => $form->createView()));
    }


    public function editAction(Request $request, Exam $exam)
    {
        $form = $this->createForm(new ExamEditType(), $exam);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $em->persist($exam);
            $em->flush();

            return $this->redirectToRoute("diplom_admin_exam_index");
        }

        return $this->render(':AppBundle/admin/exam:create.html.twig', array('form' => $form->createView()));
    }

    public function removeAction(Exam $exam)
    {
        $this->denyAccessUnlessGranted(ExamVoter::REMOVE, $exam, 'Нельзя удалить активный экзамен');

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        foreach ($exam->getReports() as $report) {
            $em->remove($report);
            $em->flush();
        }

        $em->remove($exam);
        $em->flush();

        return $this->redirectToRoute("diplom_admin_exam_index");
    }

    /**
     * На подгтовку координатору
     * @param Exam $exam
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function preparationAction(Exam $exam)
    {
        try {
            $this->get('diplom_exam_manager')->startPreparingForExam($exam);
        } catch (ExamException $e) {
            $this->get('session')->getFlashBag()->set('warning', $e->getMessage());
        }

        $this->get('session')->getFlashBag()->set('success', "Экзамен отправлен координатору");

        return $this->redirectToRoute("diplom_admin_exam_index");
    }

    /**
     * В черновик
     * @param Exam $exam
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function toDraftAction(Exam $exam)
    {
        try {
            $this->get('diplom_exam_manager')->stopExamPreparation($exam);
        } catch (ExamException $e) {
            $this->get('session')->getFlashBag()->set('warning', $e->getMessage());
        }

        $this->get('session')->getFlashBag()->set('success', "Экзамен отправлен в черновики");

        return $this->redirectToRoute("diplom_admin_exam_index");
    }

    public function toArchiveAction(Exam $exam)
    {
        try {
            $this->get('diplom_exam_manager')->sendExamToArchive($exam);
        } catch (ExamException $e) {
            $this->get('session')->getFlashBag()->set('warning', $e->getMessage());
        }

        $this->get('session')->getFlashBag()->set('success', "Экзамен отправлен в архив");

        return $this->redirectToRoute("diplom_admin_exam_index");
    }

    public function resetAction(Exam $exam)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN') === false) {
            $this->get('session')->getFlashBag()->set('warning', 'Нет прав на сброс экзамена');
            return $this->redirectToRoute('diplom_admin_exam_index');
        }

        try {
            $this->get('diplom_exam_manager')->resetExam($exam);
        } catch (IOExceptionInterface $e) {
            $this->get('session')->getFlashBag()->set('warning', $e->getMessage());
            return $this->redirectToRoute('diplom_admin_exam_index');
        }
        
        $this->get('session')->getFlashBag()->set('success', "Экзамен сброшен");
        return $this->redirectToRoute('diplom_admin_exam_index');
    }
} 