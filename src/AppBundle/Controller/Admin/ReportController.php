<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Exam;
use AppBundle\Entity\Report;
use AppBundle\Form\Type\ReportType;
use AppBundle\Security\Authorization\Voter\ReportVoter;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ReportController extends Controller
{
    public function indexAction(Exam $exam)
    {
        return $this->render(':AppBundle/admin/report:index.html.twig', array(
            'exam' => $exam,
            'reports' => $exam->getReports(),
        ));
    }

    public function createAction(Request $request, Exam $exam)
    {
        $report = new Report();
        $report->setExam($exam);

        $form = $this->createForm(new ReportType(), $report, array(
            'validation_groups' => array('create', 'Default'),
        ));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (!$report->getOrganization()) {
                $report->setOrganization($em->getRepository('AppBundle:Organization')->getFictitiousOrganization());
            }
            $em->persist($report);
            $em->flush();

            return $this->redirectToRoute("diplom_admin_exam_reports", array(
                'exam' => $exam->getId(),
            ));
        }

        return $this->render(':AppBundle/admin/report:edit.html.twig', array(
            'exam' => $exam,
            'report' => $report,
            'form' => $form->createView(),
            'title' => 'Добавление студента',
        ));
    }


    public function editAction(Request $request, Exam $exam, Report $report)
    {
        $form = $this->createForm(new ReportType(), $report);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (!$report->getOrganization()) {
                $report->setOrganization($em->getRepository('AppBundle:Organization')->getFictitiousOrganization());
            }
            $em->persist($report);
            $em->flush();

            return $this->redirectToRoute("diplom_admin_exam_reports", array(
                'exam' => $exam->getId(),
            ));
        }

        return $this->render(':AppBundle/admin/report:edit.html.twig', array(
            'exam' => $exam,
            'report' => $report,
            'form' => $form->createView(),
            'title' => 'Редактирование студента',
        ));
    }

    public function removeAction(Exam $exam, Report $report)
    {
        $this->denyAccessUnlessGranted(ReportVoter::REMOVE, $report, 'Нельзя удалить доклад активного экзамена');

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        if ($exam->getCurrentReport() && $exam->getCurrentReport()->getId() == $report->getId()) {
            $exam->setCurrentReport(null);
            $em->persist($exam);
        }

        $em->remove($report);
        $em->flush();

        return $this->redirectToRoute("diplom_admin_exam_reports", array(
            'exam' => $exam->getId(),
        ));
    }
}