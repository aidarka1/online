<?php

namespace AppBundle\Controller;

use AppBundle\Application\WampTopicNames;
use AppBundle\Entity\Report;
use AppBundle\Enum\GradeAuthorType;
use AppBundle\Enum\ReportStatus;
use AppBundle\Exception\ExamException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CoordinatorController extends Controller
{
    public function indexAction($role)
    {
        $exam = null;
        $examManager = $this->get('diplom_exam_manager');
        try {
            if ($this->get('security.authorization_checker')->isGranted('ROLE_COORDINATOR')) {
                $exam = $examManager->getExamForCoordinator();
            } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CHAIRMAN')) {
                $exam = $examManager->getActiveExam();
            }
        } catch (ExamException $e) {
            if ($this->get('security.authorization_checker')->isGranted('ROLE_COORDINATOR')) {
                $topics = array(WampTopicNames::waitExamPreparation() => 'waitExamPreparation');
            } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_CHAIRMAN')) {
                $topics = array(WampTopicNames::waitExamStart() => 'waitExamStart');
            } else {
                $topics = array();
            }

            return $this->render(':AppBundle:index_wait.html.twig', array(
                'topics' => json_encode($topics),
                'websocket_port' => $this->container->getParameter('websocket_listen_port'),
            ));
        }

        $nextReport = $examManager->getNextAwaitingReportOrNullResult($exam);
        $nextNextReport = $examManager->getNextAwaitingReportOrNullResult($exam,1);

        $exam_params = array(
            'id' => $exam->getId(),
            'name' => $exam->getName(),
            'status' => $exam->getStatus(),
            'nextStudentFio' => $nextReport ? $nextReport->getStudent() : '',
            'nextTheme' => $nextReport ? $nextReport->getName() : '',
            'nextNextStudentFio' => $nextNextReport ? $nextNextReport->getStudent() : '',
        );
        // Постоянная подписка. От этих топиков не отписываемся при смене доклада
        $invariable_topics = array(
            WampTopicNames::switchExamReportForCoordinator($exam) => 'switchExamReport',
            WampTopicNames::waitExamEnd($exam) => 'waitExamEnd',
        );

        try {
            $report = $examManager->getActiveReport($exam);
            $questions = array();
            foreach ($report->getQuestions() as $question) {
                $questions[] = array(
                    'id' => $question->getId(),
                    'text' => $question->getText(),
                    'author' => $question->getUser()->getUsername(),
                );
            }

            $grades = array();
            $grade_data = $examManager->getGradesByReport($report);
            foreach ($grade_data as $type => $type_data) {
                foreach ($type_data as $author_type => $grade) {
                    if ($author_type !== GradeAuthorType::COMMISSION && $grade !== null) {
                        $grades[] = array(
                            'type' => $type,
                            'author' => $author_type,
                            'grade' => $grade
                        );
                    }
                }
            }

            $grade_repo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Grade');
            $report_params = array(
                'id' => $report->getId(),
                'name' => $report->getName(),
                'status' => $report->getStatus() == ReportStatus::BUILD_PROTOCOL && !$grade_repo->isCommissionGradesExist($report) ? 'commission_grades' : $report->getStatus(),
                'studentFio' => $report->getStudent(),
                'questions' => $questions,
                'grades' => $grades,
            );
            $topics = array(
                WampTopicNames::switchReportStatus($report) => 'switchReportStatus',
                WampTopicNames::getQuestionsByReport($report) => 'getQuestionsByReport',
                WampTopicNames::getReportGrades($report) => 'getReportGrades',
            );
        } catch (ExamException $e) {
            $report_params = null;
            $topics = array();
        }

        return $this->render(':AppBundle/coordinator:index.html.twig', array(
            'role' => $role,
            'invariable_topics' => json_encode($invariable_topics),
            'topics' => json_encode($topics),
            'examParams' => json_encode($exam_params),
            'reportParams' => json_encode($report_params),
            'websocket_port' => $this->container->getParameter('websocket_listen_port'),
        ));
    }

    public function showProtocolAction(Report $report)
    {
        $protocol_builder = $this->get('diplom_report_protocol');
        $protocol_builder->showProtocolByReport($report);die;
    }
}
