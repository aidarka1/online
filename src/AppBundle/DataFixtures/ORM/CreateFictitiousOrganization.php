<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Organization;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Создает фиктивную организацию.
 * С этой организацией будут связываться доклады, у которых реально организация не выбрана.
 * Class CreateFictitiousOrganization
 * @package AppBundle\DataFixtures\ORM
 */
class CreateFictitiousOrganization extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $organization = new Organization();
        $organization->setExpert($this->getReference('fictitious-user'));
        $organization->setName('-');
        $organization->setVisible(false);
        $organization->setPosition(0);

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($organization);
        $em->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}