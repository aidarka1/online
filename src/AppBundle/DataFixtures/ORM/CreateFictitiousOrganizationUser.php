<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Создает пользователя с ролью суперадмина для связывания с фиктивной организацией
 * Class CreateFictitiousOrganizationUser
 * @package AppBundle\DataFixtures\ORM
 */
class CreateFictitiousOrganizationUser extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $user_manager = $this->container->get('fos_user.user_manager');

        /** @var User $user */
        $user = $user_manager->createUser();
        $user->setUsername('Fictitious User');
        $user->setEmail('robot@ec-univer.ru');
        $user->setPlainPassword('fgh3HQ432vd!');
        $user->setRoles(array('ROLE_SUPER_ADMIN'));

        $user_manager->updateUser($user);

        $this->addReference('fictitious-user', $user);
    }

    public function getOrder()
    {
        return 1;
    }
}