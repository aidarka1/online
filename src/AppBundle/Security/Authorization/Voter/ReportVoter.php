<?php

namespace AppBundle\Security\Authorization\Voter;

use AppBundle\Entity\Report;
use AppBundle\Enum\ExamStatus;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ReportVoter implements VoterInterface
{
    const REMOVE = 'remove';

    public function supportsAttribute($attribute)
    {
        return in_array($attribute, array(
            self::REMOVE,
        ));
    }

    public function supportsClass($class)
    {
        $supported_class = 'AppBundle\Entity\Report';
        return $supported_class === $class || is_subclass_of($class, $supported_class);
    }

    /**
     * @param TokenInterface $token
     * @param Report $report
     * @param array $attributes
     * @return int
     */
    public function vote(TokenInterface $token, $report, array $attributes)
    {
        if (!$this->supportsClass(get_class($report))) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        if (1 !== count($attributes)) {
            throw new \InvalidArgumentException('Доступен только 1 атрибут');
        }

        $attribute = $attributes[0];

        if (!$this->supportsAttribute($attribute)) {
            return VoterInterface::ACCESS_ABSTAIN;
        }

        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            return VoterInterface::ACCESS_DENIED;
        }

        switch ($attribute) {
            case self::REMOVE:
                if (!array_key_exists($report->getExam()->getStatus(), ExamStatus::getActiveStatuses())) {
                    return VoterInterface::ACCESS_GRANTED;
                }
                break;
        }

        return VoterInterface::ACCESS_DENIED;
    }
}