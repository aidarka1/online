<?php

namespace AppBundle\Entity;

use Doctrine\ORM\QueryBuilder;
use Gedmo\Sortable\Entity\Repository\SortableRepository;

class OrganizationRepository extends SortableRepository
{
    /**
     * Возвращает фиктивную организацию.
     * В случае, если такой организации нет, либо их несколько выбросит исключение
     * Фиктивная организация создается через фикстуры
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getFictitiousOrganization()
    {
        return $this->createQueryBuilder('o')
            ->where('o.visible = false')
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Возвращает QueryBuilder для поиска видимых организаций
     * @return QueryBuilder
     */
    public function getVisibleOrganizationsQueryBuilder()
    {
        return $this->createQueryBuilder('o')
            ->where('o.visible = true')
            ->orderBy('o.position', 'ASC');
    }

    /**
     * Возвращает только видимые организации
     * @return Organization[]
     */
    public function getVisibleOrganizations()
    {
        return $this->getVisibleOrganizationsQueryBuilder()
            ->getQuery()
            ->getResult();
    }
}