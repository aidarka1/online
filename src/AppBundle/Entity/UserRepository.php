<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class UserRepository extends EntityRepository
{
    /**
     * Возвращает всех суперпользователей системы
     * @return User[]
     */
    public function getSuperAdmins()
    {
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->where($qb->expr()->like('u.roles', $qb->expr()->literal('%ROLE_SUPER_ADMIN%')))
            ->getQuery()
            ->getResult();
    }

    /**
     * Возвращает всех доступных пользователей отсортированных по имени
     * @return User[]
     */
    public function findAvailableUsersOrderedByName()
    {
        return $this->getQueryBuilderWithEnabledUnLockedCondition()
            ->orderBy('u.username', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Проверка, что активный пользователь (активный и не заблокированный) с ролью ROLE_CORPORATE_EXPERT существует
     * В системе может быть только 1 пользователь с такой ролью
     * @return bool
     */
    public function isCorporateExpertExist()
    {
        return (bool) count($this->findUsersWithRole('ROLE_CORPORATE_EXPERT'));
    }

    /**
     * Возвращает активных пользователей с ролью "Эксперт предприятия" отсортированных по имени
     * Если требуется получить объект класса QueryBuilder необходимо передать параметр $query_builder
     * @param bool $query_builder
     * @return User[]|QueryBuilder
     */
    public function findOrganizationExpertsOrderedByName($query_builder = false)
    {
        return $query_builder === true
            ? $this->getUsersWithRoleQueryBuilder('ROLE_ORGANIZATION_EXPERT')
            : $this->findUsersWithRole('ROLE_ORGANIZATION_EXPERT');
    }

    /**
     * Возвращает запрос на получение свободных экспертов
     * (пользователи с ролью "эксперт", которые не привязаны ни к одной организации)
     * В случае редактирования организации (связана с экспертом), в списке должен присутствовать помимо
     * свободных пользователей также и текущий эксперт. Для этого есть параметр $current_organization
     * @params Organization $current_organization Организация, для которой ищем свободных экспертов
     * @return QueryBuilder
     */
    public function findAvailableOrganizationExpertsQueryBuilder(Organization $current_organization = null)
    {
        $query_builder = $this->getUsersWithRoleQueryBuilder('ROLE_ORGANIZATION_EXPERT')
            ->leftJoin('u.expertOrganization', 'o');

        if ($current_organization !== null && $current_organization->getId()) {
            $query_builder
                ->andWhere($query_builder->expr()->isNull('o.id') . ' OR o.expert != :current_organization')
                ->setParameter('current_organization', $current_organization);
        } else {
            $query_builder->andWhere($query_builder->expr()->isNull('o.id'));
        }

        return $query_builder;
    }

    /**
     * Возвращает активных пользователей с ролью "Координатор" отсортированных по имени
     * Если требуется получить объект класса QueryBuilder необходимо передать параметр $query_builder
     * @param bool $query_builder
     * @return User[]|QueryBuilder
     */
    public function findCoordinatorsOrderedByName($query_builder = false)
    {
        return $query_builder === true
            ? $this->getUsersWithRoleQueryBuilder('ROLE_COORDINATOR')
            : $this->findUsersWithRole('ROLE_COORDINATOR');
    }

    /**
     * Возвращает активных пользователей с ролью "Председатель" отсортированных по имени
     * Если требуется получить объект класса QueryBuilder необходимо передать параметр $query_builder
     * @param bool $query_builder
     * @return User[]|QueryBuilder
     */
    public function findChairmansOrderedByName($query_builder = false)
    {
        return $query_builder === true
            ? $this->getUsersWithRoleQueryBuilder('ROLE_CHAIRMAN')
            : $this->findUsersWithRole('ROLE_CHAIRMAN');
    }

    /**
     * Возвращает пользователей с заданной ролью (Суперадмин исключается)
     * @param string $role
     * @return User[]
     */
    private function findUsersWithRole($role)
    {
        return $this->getUsersWithRoleQueryBuilder($role)
            ->getQuery()
            ->getResult();
    }

    /**
     * Возвращает базовый QueryBuilder дополненный условием по наличию у пользователя заданной роли
     * @param string $role
     * @return QueryBuilder
     */
    private function getUsersWithRoleQueryBuilder($role)
    {
        $qb = $this->getQueryBuilderWithEnabledUnLockedCondition();
        return $qb->andWhere($qb->expr()->like('u.roles', $qb->expr()->literal('%' . $role . '%')));
    }

    /**
     * Возвращает базовый QueryBuilder
     * Исключены не активные и заблокированные пользователи. Также пользователи с ролью "Суперадмин"
     * @return QueryBuilder
     */
    private function getQueryBuilderWithEnabledUnLockedCondition()
    {
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->where('u.enabled = true AND u.locked = false')
            ->andWhere($qb->expr()->notLike('u.roles', $qb->expr()->literal('%ROLE_SUPER_ADMIN%')))
            ->orderBy('u.username', 'ASC');
    }
}