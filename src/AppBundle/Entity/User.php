<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Exam", mappedBy="chairman")
     * @var Exam[]
     */
    protected $chairmanExams;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Exam", mappedBy="coordinator")
     * @var Exam[]
     */
    protected $coordinatorExams;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Question", mappedBy="user")
     * @var Question[]
     */
    protected $questions;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Grade", mappedBy="user")
     * @var Grade[]
     */
    protected $grades;
    /**
     * Организация, в которой пользователь является Экспертом
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Organization", mappedBy="expert")
     *
     * @var Organization
     */
    private $expertOrganization;

    public function __construct()
    {
        parent::__construct();
        $this->chairmanExams = new ArrayCollection();
        $this->coordinatorExams = new ArrayCollection();
        $this->questions = new ArrayCollection();
        $this->grades = new ArrayCollection();
    }

    /**
     * @return Organization
     */
    public function getExpertOrganization()
    {
        return $this->expertOrganization;
    }

    /**
     * @param Organization $expertOrganization
     */
    public function setExpertOrganization($expertOrganization)
    {
        $this->expertOrganization = $expertOrganization;
    }

    /**
     * @return Exam[]
     */
    public function getChairmanExams()
    {
        return $this->chairmanExams;
    }

    /**
     * @return mixed
     */
    public function getCoordinatorExams()
    {
        return $this->coordinatorExams;
    }

    /**
     * @return Grade[]
     */
    public function getGrades()
    {
        return $this->grades;
    }

    /**
     * @return Question[]
     */
    public function getQuestions()
    {
        return $this->questions;
    }
}