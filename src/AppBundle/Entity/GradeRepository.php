<?php

namespace AppBundle\Entity;

use AppBundle\Enum\GradeAuthorType;
use Doctrine\ORM\EntityRepository;

class GradeRepository extends EntityRepository
{
    public function getGradesAsArrayByReport(Report $report)
    {
        return $this->createQueryBuilder('g')
            ->where('g.report = :report')
            ->setParameter('report', $report)
            ->getQuery()
            ->getArrayResult();
    }

    public function isCommissionGradesExist(Report $report)
    {
        $grades_array = $this->createQueryBuilder('g')
            ->where('g.report = :report')
            ->andWhere('g.authorType = :author_type')
            ->setParameter('report', $report)
            ->setParameter('author_type', GradeAuthorType::COMMISSION)
            ->getQuery()
            ->getArrayResult();

        return count($grades_array) > 0;
    }
}