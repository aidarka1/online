<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\OrganizationRepository")
 * @ORM\Table(name="organization")
 * */
class Organization
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $name;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * Эксперт организации
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User", inversedBy="expertOrganization")
     * @ORM\JoinColumn(name="expert_id", referencedColumnName="id")
     *
     * @var User
     */
    private $expert;

    /**
     * Эксперт организации
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Report", mappedBy="organization")
     * @ORM\JoinColumn(name="expert_id", referencedColumnName="id")
     *
     * @var Report[]
     */
    private $students;

    /**
     * Видна ли организация
     *
     * @Gedmo\SortableGroup
     * @ORM\Column(type="boolean")
     */
    private $visible = true;

    public function __construct()
    {
        $this->students = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return User
     */
    public function getExpert()
    {
        return $this->expert;
    }

    /**
     * @param User $expert
     */
    public function setExpert(User $expert)
    {
        $this->expert = $expert;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \AppBundle\Entity\Report[]
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Organization
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }
}