<?php
namespace AppBundle\Entity;

use AppBundle\Enum\GradeAuthorType;
use AppBundle\Enum\GradeType;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GradeRepository")
 * @ORM\Table(name="grade")
 */
class Grade
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\Column(type="string")
     */
    private $authorType;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Report", inversedBy="grades")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id")
     * @var Report
     */
    private $report;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="grades")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @var User
     */
    private $user;

    /**
     * @ORM\Column(name="create_date",type="datetime")
     */
    private $createDate;

    /**
     * @var integer
     * @ORM\Column(name="value", type="decimal", precision=1, scale=2)
     */
    private $value;

    function __construct()
    {
        $this->createDate = new DateTime();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getAuthorType()
    {
        return $this->authorType;
    }

    /**
     * @param string $authorType
     */
    public function setAuthorType($authorType)
    {
        $this->authorType = $authorType;
    }

    /**
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param Report $report
     */
    public function setReport(Report $report)
    {
        $this->report = $report;
    }


    /**
     * @return DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function callbackGetAllTypes()
    {
        return array_keys(GradeType::getChoices());
    }

    public function callbackGetAllAuthorTypes()
    {
        return array_keys(GradeAuthorType::getChoices());
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }
}