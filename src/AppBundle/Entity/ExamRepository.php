<?php

namespace AppBundle\Entity;

use AppBundle\Enum\ExamStatus;
use Doctrine\ORM\EntityRepository;

class ExamRepository extends EntityRepository
{
    /**
     * Возвращает активный экзамен
     * @return Exam
     */
    public function getActiveExam()
    {
        return $this->createQueryBuilder('e')
            ->where('e.status = :awaiting_status')
            ->orWhere('e.status = :action_status')
            ->setParameter('awaiting_status', ExamStatus::AWAITING_REPORT)
            ->setParameter('action_status', ExamStatus::SPEECH)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Возвращает экзамен, который не в подготовке и не в архиве
     * @return Exam
     */
    public function getExamNotInPreparationAndDraftAndArchive()
    {
        $qb = $this->createQueryBuilder('e');
        return $qb
            ->select('e')
            ->where($qb->expr()->notIn('e.status', [ExamStatus::PREPARATION, ExamStatus::DRAFT, ExamStatus::ARCHIVE]))
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Проверка на наличие активного экзамена
     * @return bool
     */
    public function isActiveExamExist()
    {
        return $this->getActiveExam() !== null;
    }
}