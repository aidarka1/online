<?php
namespace AppBundle\Entity;

use AppBundle\Enum\ReportStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="report")
 * @ORM\Entity(repositoryClass="Gedmo\Sortable\Entity\Repository\SortableRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Report
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="string")
     */
    private $student;

    /**
     * @Gedmo\SortableGroup
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Exam", inversedBy="reports")
     * @ORM\JoinColumn(name="exam_id", referencedColumnName="id")
     * @var Exam
     */
    private $exam;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Grade", mappedBy="report", cascade={"remove"})
     * @var Grade[]
     */
    private $grades;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Question", mappedBy="report", cascade={"remove"})
     * @var Question[]
     */
    private $questions;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $faculty;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $specialty;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $specialization;

    /**
     * @var string
     * @ORM\Column(type="string", name="group_number")
     */
    private $groupNumber;

    /**
     * @var Organization
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Organization", inversedBy="students")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     */
    private $organization;

    /**
     * @var string
     * @ORM\Column(type="string", name="department_head")
     */
    private $departmentHead;

    /**
     * @var string
     * @ORM\Column(type="string", name="company_head", nullable=true)
     */
    private $companyHead;

    /**
     * @var string
     * @ORM\Column(type="string", name="phone_number")
     */
    private $phoneNumber;

    /**
     * @var string
     * @ORM\Column(type="string", name="average_grade")
     */
    private $averageGrade;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @var UploadedFile
     */
    private $file;

    private $temp;

    function __construct()
    {
        $this->status = ReportStatus::AWAITING;
        $this->grades = new ArrayCollection();
        $this->questions = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string $student
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * @return Exam
     */
    public function getExam()
    {
        return $this->exam;
    }

    /**
     * @param $student
     */
    public function setStudent($student)
    {
        $this->student = $student;
    }

    /**
     * @param Exam $exam
     */
    public function setExam(Exam $exam)
    {
        $this->exam = $exam;
    }

    public function callbackGetAllStatuses()
    {
        return array_keys(ReportStatus::getChoices());
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : '/'.$this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/user/photo';
    }

    /**
     * Sets photo.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->path)) {
            // store the old name to delete after the update
            $this->temp = $this->path;
            $this->path = null;
        } else {
            $this->path = 'initial';
        }
    }

    /**
     * Get photo.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
    return $this->file;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename.'.'.$this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the photo, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->path);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    /**
     * @param string $companyHead
     */
    public function setCompanyHead($companyHead)
    {
        $this->companyHead = $companyHead;
    }

    /**
     * @return string
     */
    public function getCompanyHead()
    {
        return $this->companyHead;
    }

    /**
     * @param string $departmentHead
     */
    public function setDepartmentHead($departmentHead)
    {
        $this->departmentHead = $departmentHead;
    }

    /**
     * @return string
     */
    public function getDepartmentHead()
    {
        return $this->departmentHead;
    }

    /**
     * @param string $faculty
     */
    public function setFaculty($faculty)
    {
        $this->faculty = $faculty;
    }

    /**
     * @return string
     */
    public function getFaculty()
    {
        return $this->faculty;
    }

    /**
     * @param string $groupNumber
     */
    public function setGroupNumber($groupNumber)
    {
        $this->groupNumber = $groupNumber;
    }

    /**
     * @return string
     */
    public function getGroupNumber()
    {
        return $this->groupNumber;
    }

    /**
     * @param \AppBundle\Entity\Organization $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
    }

    /**
     * @return \AppBundle\Entity\Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $specialization
     */
    public function setSpecialization($specialization)
    {
        $this->specialization = $specialization;
    }

    /**
     * @return string
     */
    public function getSpecialization()
    {
        return $this->specialization;
    }

    /**
     * @param string $specialty
     */
    public function setSpecialty($specialty)
    {
        $this->specialty = $specialty;
    }

    /**
     * @return string
     */
    public function getSpecialty()
    {
        return $this->specialty;
    }

    /**
     * @return Grade[]
     */
    public function getGrades()
    {
        return $this->grades;
    }

    /**
     * @return Question[]
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param string $averageGrade
     */
    public function setAverageGrade($averageGrade)
    {
        $this->averageGrade = $averageGrade;
    }

    /**
     * @return string
     */
    public function getAverageGrade()
    {
        return $this->averageGrade;
    }
}