<?php
namespace AppBundle\Entity;

use AppBundle\Enum\ExamStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ExamRepository")
 * @ORM\Table(name="exam")
 */
class Exam
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="chairmanExams")
     * @ORM\JoinColumn(name="chairman_id", referencedColumnName="id")
     * @var User
     */
    private $chairman;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="coordinatorExams")
     * @ORM\JoinColumn(name="coordinator_id", referencedColumnName="id")
     * @var User
     */
    private $coordinator;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Report", mappedBy="exam", cascade={"persist", "remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     * @var Report[]
     */
    private $reports;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Report")
     * @ORM\JoinColumn(name="current_report_id", referencedColumnName="id", nullable=true)
     * @var Report
     */
    private $currentReport;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $delay = 0;


    function __construct()
    {
        $this->reports = new ArrayCollection();
        $this->status = ExamStatus::DRAFT;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return User
     */
    public function getChairman()
    {
        return $this->chairman;
    }

    /**
     * @param User $chairman
     */
    public function setChairman(User $chairman)
    {
        $this->chairman = $chairman;
    }

    /**
     * @return User
     */
    public function getCoordinator()
    {
        return $this->coordinator;
    }

    /**
     * @param User $coordinator
     */
    public function setCoordinator(User $coordinator)
    {
        $this->coordinator = $coordinator;
    }

    /**
     * @return ArrayCollection|Report[]
     */
    public function getReports()
    {
        return $this->reports;
    }

    /**
     * @return Report
     */
    public function getCurrentReport()
    {
        return $this->currentReport;
    }

    /**
     * @param Report $currentReport
     */
    public function setCurrentReport($currentReport)
    {
        $this->currentReport = $currentReport;
    }

    public function getUploadScreenRootDir()
    {
        return __DIR__.'/../../../web/'.$this->getUploadScreenDir();
    }

    public function getUploadScreenDir()
    {
        return 'uploads/screens/'.$this->getId();
    }

    /**
     * @param $delay
     */
    public function setDelay($delay)
    {
        $this->delay = $delay;
    }

    /**
     * @return int
     */
    public function getDelay()
    {
        return $this->delay;
    }

    public function addReport(Report $report)
    {
        $report->setExam($this);

        $this->reports->add($report);
    }

    public function removeReport(Report $report)
    {
        $this->reports->removeElement($report);
    }

    public function callbackGetAllStatuses(){
        return array_keys(ExamStatus::getChoices());
    }

}