var transport = new DesktopTransport();
var __extends = this.__extends || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        __.prototype = b.prototype;
        d.prototype = new __();
};

/**
 * Родительский класс экзамена
 */
var Exam = (function () {
    function Exam(options) {
        this.id = options.id;
        this.name = options.name;
        this.status = options.status;
        this.currentReport = options.currentReport;
    }

    /**
     * Отрисовка отчета
     */
    Exam.prototype.drawReport = function(report){
        $.modal().close();
        this.showTitles(report.name, report.studentFio);
    };

    Exam.prototype.clearDesktop = function(options) {
        this.showTitles(null, null);
        $('#questionList').empty();
    };

    /**
     * Показать список вопросов текущего доклада
     * @param report
     */
    Exam.prototype.showQuestions = function(report){
        $('#questionList').empty();
        for (var i = 0; i < report.questions.length; i++) {
            this.questionShow(report.questions[i])
        }
    };

    /**
     * Показать верхние заголовки (Тема и Фио студента)
     * @param theme
     * @param fio
     */
    Exam.prototype.showTitles = function (theme, fio) {
        theme = theme || 'Ожидается следующая тема';
        fio = fio || 'Ожидается следующий студент';
        $('#reportTitle').html(theme);
        $('#studentFio').html(fio);
    };


    /*--- Начало. Функции обработчики socket сигналов ---*/
    Exam.prototype.reportToDiscussion = function() {
        this.currentReport.status = 'discussion';
        this.drawReport(this.currentReport);
        this.wasGraded = null;
        this.wasAsked = null;
    };

    Exam.prototype.reportToProtocol = function() {
        this.currentReport.status = 'commission_grades';
        this.drawReport(this.currentReport);
    };

    Exam.prototype.reportToComplete = function() {
        this.currentReport = null;
        this.clearDesktop();
    };

    Exam.prototype.reportNew = function(reportData) {
        this.status = 'speech';
        var report = new Report(reportData);
        this.currentReport = report;
        this.drawReport(report);
    };

    Exam.prototype.questionNew = function(options) {
        this.currentReport.questions.push(options);
        this.questionShow(options);
        var $questionListContainer = $('#questionListContainer');
        $questionListContainer.mCustomScrollbar('update');
        $questionListContainer.mCustomScrollbar("scrollTo", "bottom", {
            scrollInertia: 1500
        });
        //this.showQuestions(this.currentReport);
    };
    /*--- Конец Функции обработчики socket сигналов ---*/

    return Exam;
})();

/**
 *  Класс экзамена для зрителя
 */
var ExamForViewer = (function (_super) {
    __extends(ExamForViewer, _super);

    var screenContainer = $("#screenContainer");

    function ExamForViewer(options) {
        this.videoIp = options.videoIp;
        this.videoPort = options.videoPort;
        this.videoStream = options.videoStream;
        this.videoPath = options.videoPath;
        this.videoSwfPath = options.videoSwfPath;
        this.videoPoster = options.videoPoster;
        this.presentationPoster = options.presentationPoster;

        _super.call(this, options);

        this.showVideo();
    }

    ExamForViewer.prototype.clearDesktop = function(options) {
        _super.prototype.clearDesktop(options);
        screenContainer.css({
            'background-image': 'url(' + this.presentationPoster + ')'
        });
    };

    /**
     * Отрисовка отчета
     */
    ExamForViewer.prototype.drawReport = function(report){
        _super.prototype.drawReport(report);
        this.showQuestions(report);
        this.screenShotNew(report);
    };


    /**
     * Отробразить последний скрин с презентации
     * @param report
     */
    ExamForViewer.prototype.screenShotNew = function(report) {
        if (report || this.currentReport) {
            transport.addAction(Routing.generate("diplom_ajax_screen_load"), null, loadScreenCallback);
        }
    };

    /**
     * Обработчик ajax запроса на получение изображения
     * @param result
     */
    var loadScreenCallback = function(result) {
        if (result.status == 'ok' && result.data.length > 0) {
            var $img = $(new Image());
            $img.attr('src', '/' + result.data).load(function() {
                screenContainer.css({
                    'background-image': 'url("/'+result.data+'")'
                });
            });
        }
    };

    /**
     * Показать окно с видео
     */
    ExamForViewer.prototype.showVideo = function(){
        var _this = this;
        $("#jquery_jplayer_1").jPlayer({
            size: {height: '240px', width: '320px'},
            ready: function () {
                $(this).jPlayer("setMedia", {
                    title: "Видео защиты диплома",
                    rtmpv: "rtmp://" + _this.videoIp + ":" + _this.videoPort + "/" + _this.videoPath + "/" + _this.videoStream,
                    m4v: "http://" + _this.videoIp + ":" + _this.videoPort + "/" + _this.videoPath + "/mp4:" + _this.videoStream + "/playlist.m3u8",
                    poster: _this.videoPoster
                });
            },
            cssSelectorAncestor: "#jp_container_1",
            swfPath: this.videoSwfPath,
            solution: 'flash, html',
            supplied: "rtmpv, m4v"
        });
    };

    /**
     * Функция обработчик socket сигнала о новом вопросе
     * @param options
     */
    ExamForViewer.prototype.questionShow = function(options) {
        $('#questionList').append(
            '<div class="present_body-list-item">' +
            '<span class="c_success">' + options.author + ':</span> ' + options.text +
            '</div>'
        );
    };

    return ExamForViewer;
})(Exam);


/**
 *  Класс экзамена для эксперта
 */
var ExamForExpert = (function (_super) {
    __extends(ExamForExpert, _super);

    // панель создания вопроса
    var questionBar = $("#addQuestion");
    var gradeBar = $("#addGrade");

    function ExamForExpert(options) {
        this.userOrganizationId = options.userOrganizationId;
        this.isCorporateExpert = options.isCorporateExpert;
        this.wasGraded = options.wasGraded;
        this.wasAsked = options.wasAsked;
        _super.call(this, options);

        /*** Listeners dom elements  ***/
        //добавление вопроса
        $("#add-question-btn").on("click", function(e){
            e.preventDefault();
            var question = $(".present-control").val();
            if (question.length > 0) {
                ExamForExpert.prototype.addQuestionToServer(question);
            }
        });

        //добавление оценки
        $("#addGradeLink").on("click", function() {
            $('#modal').modal().open();
            return false;
        });

        $("#addGradeLinkInactive").on("click", function(event) {
            event.preventDefault();
            return false;
        });

        $("#add-question-btn-inactive").on("click", function(event) {
            event.preventDefault();
            return false;
        });

        //открыть закрыть модальные окна
        $('.ad_modal-close, .cm_btn-danger').on('click', function(e){
            e.preventDefault();
            $.modal().close();
        });

        var __self = this;

        //отправка оценок экспертам
        $("form[name=ExamExpertType]").on("submit", function(e){
            e.preventDefault();
            transport.addAction(Routing.generate("diplom_ajax_report_add_grades", {id: __self.currentReport.id}), $(this).serialize(), $.proxy(__self.addGradeCallback, __self));
        })
    }

    /**
     * Отрисовка отчета
     */
    ExamForExpert.prototype.drawReport = function(report){
        _super.prototype.drawReport(report);

        questionBar.show();
        gradeBar.show();

        if (report.status == 'discussion' && this.wasAsked !== '1') {
            this.showAddQuestionPanel();
        }else{
            this.hideAddQuestionPanel();
        }
        if (report.status == 'discussion' && (report.organizationId == this.userOrganizationId || this.isCorporateExpert == "1") && this.wasGraded !== '1') {
            this.showAddGradePanel();
        }else{
            this.hideAddGradePanel();
        }
    };

    //Отправить запрос на сервер "добавление вопроса"
    ExamForExpert.prototype.addQuestionToServer = function(question){
        transport.addAction(Routing.generate("diplom_ajax_question_add"), {text: question}, addQuestionsCallback);
        this.hideAddQuestionPanel();
    };
    //Скртыть панель, где можно задать вопрос
    var addQuestionsCallback = function(result) {
        //questionBar.find("button").removeClass("present_action-btn-success");
        //questionBar.find("i").removeClass("fa-paper-plane").addClass("fa-pencil");
        //questionBar.find("input").val(result.data);
    };
    //скрыть панель для оценок
    ExamForExpert.prototype.addGradeCallback = function(result) {
        if (result.errors !== null && typeof result.errors === 'object') {
            $.each(result.errors, function(k, v){
                $("form[name=ExamExpertType]").eq(0).find("#ExamExpertType_" + k).after("<span class='error'>" + v + "</span>");
            });
        } else {
            $.modal().close();
            this.hideAddGradePanel();
        }
    };

    //Отобразить панель, где можно задать вопрос
    ExamForExpert.prototype.showAddQuestionPanel = function(){
        $('#add-question-btn').show();
        $('#add-question-btn-inactive').hide();
    };

    //Отобразить панель, где можно задать вопрос
    ExamForExpert.prototype.hideAddQuestionPanel = function(){
        $('#add-question-btn').hide();
        $('#add-question-btn-inactive').show();
    };

    //Отобразить панель, где можно добавить оценку
    ExamForExpert.prototype.showAddGradePanel = function(){
        $('#addGradeLink').show();
        $('#addGradeLinkInactive').hide();
    };
    //Скртыть панель, где можно добавить оценку
    ExamForExpert.prototype.hideAddGradePanel = function(){
        $('#addGradeLink').hide();
        $('#addGradeLinkInactive').show();
        $("form").find("input[type=text]").val('');
    };
    return ExamForExpert;
})(ExamForViewer);

var ExamForChairman = (function (_super) {
    __extends(ExamForChairman, _super);

    function ExamForChairman(options) {
        this.nextTheme = options.nextTheme;
        this.nextStudentFio = options.nextStudentFio;
        this.nextNextStudentFio = options.nextNextStudentFio;
        _super.call(this, options);
    }
    var $table = $('#table-grades');

    ExamForChairman.prototype.clearDesktop = function(options) {
        _super.prototype.clearDesktop(options);
        this.clearGrades();
    };

    /**
     * Очистить таблицу оценок
     */
    ExamForChairman.prototype.clearGrades = function(){
        $table.find('tr[class^=grade]').find('.all-center').empty();
    };

    // Округление числа до нужного знака после запятой (n)
    var round = function(x, n) {
        if (isNaN(x) || isNaN(n)) {
            return false;
        }
        var m = Math.pow(10, n);
        return Math.round(x*m)/m;
    };

    // Пересчет средней оценки для строки
    var calculateAverageGradeValue = function($row) {
        var average_sum = 0;
        var average_count = 0;
        $row.find('.all-center').each(function() {
            if (!$(this).hasClass('average')) {
                var value = parseFloat($(this).html());
                if (!isNaN(value)) {
                    average_count++;
                    average_sum += value;
                }
            }
        });
        $row.find('.average').html(round(average_sum/average_count, 1));
    };

    // Пересчет суммы для столбца с заданным типом автора
    var calculateTotalGrade = function(authorType) {
        var $column = $table.find('.total_' + authorType);
        var sum = 0;
        $table.find('.' + authorType).each(function() {
            var value = parseFloat($(this).html());
            if (!isNaN(value)) {
                sum += value;
            }
        });
        $column.html(round(sum, 1));
        calculateAverageGradeValue($table.find('.grade-total'));
    };

    ExamForChairman.prototype.drawReport = function(report){
        _super.prototype.drawReport(report);
        this.showQuestions(report);
    };


    /**
     * Функция обработчик socket сигнала о новой оценке
     * @param options
     */
    ExamForChairman.prototype.gradeNew = function(options) {
        var $type = $('.grade-' + options.type);
        var authorType = options.author;

        // Устанавливаем оценку, пришедшую с сервера
        $type.find('.' + authorType).html(options.grade);

        calculateAverageGradeValue($type);
        calculateTotalGrade(authorType);
    };

    /**
     * Функция обработчик socket сигнала о новом вопросе
     * @param options
     */
    ExamForChairman.prototype.questionShow = function(options) {
        $('#questionList').append(
            '<tr>' +
                '<td class="gx_tab_label">' + options.author + '</td>' +
                '<td><a class="gx_tab-sub-title" href="#">' + options.text + '</a></td>' +
            '</tr>'
        );
    };

    return ExamForChairman;
})(Exam);

//Для работы с координатором
var ExamForCoordinator = (function (_super) {
    __extends(ExamForCoordinator, _super);

    function ExamForCoordinator(options) {
        _super.call(this, options);
        var __self = this;

        if(!this.currentReport){
            $('#bottomControlPanel').show();
        }
        $('#bottomControlPanel').show();

        /*** Listeners dom elements  ***/
        $("#sortReportsButton").on("click", function(e){
            e.preventDefault();
            ExamForCoordinator.prototype.openReportSortModal();
        });

        $('.ad_modal-close').on('click', function(e){
            e.preventDefault();
            $.modal().close();
        });

        //Listner клика на кнопку "Начать Экзамен"
        $('#startExamButtonContainer').on('click', function(e){
            e.preventDefault();
            $(this).hide();
            transport.addAction(Routing.generate("diplom_ajax_start_exam",{id: __self.id}),null, $.proxy(__self.startExamHandler, __self));
        });
        //Listner клика на кнопку "Начать Доклад"
        $('#startReportButton').on('click', function(e){
            e.preventDefault();
            $(this).hide();
            transport.addAction(Routing.generate("diplom_ajax_start_report",{id: __self.id}),null, function(){$('#endExamButtonContainer').hide()});
        });
        //Listner клика на кнопку "Перейти к обсуждению"
        $('#reportToDiscussionButton').on('click', function(e){
            e.preventDefault();
            $(this).hide();
            transport.addAction(Routing.generate("diplom_ajax_report_to_discussion",{id: __self.id}));
        });
        //Listner клика на кнопку "Закончить доклад/Сформировать протокол"
        $('#reportToProtocolButton').on('click', function(e){
            e.preventDefault();
            $(this).hide();
            transport.addAction(Routing.generate("diplom_ajax_report_to_protocol",{id: __self.id}));
        });
        $('#protocolButton').on('click', function(e) {
            $(this).hide();
            $('#endReportButton').show();
        });

        $(document).on('keypress', "input.form-control", function(event) {
            // Нажатие запятой подменяется на точку
            var key = event.which ? event.which : event.keyCode;

            if (key == 44) {
                var Sender = $(this).get(0);

                if (document.selection) {
                    var range = document.selection.createRange();
                    range.text = '.';
                } else if (Sender.selectionStart || Sender.selectionStart == '0') {
                    var start = Sender.selectionStart;
                    var end   = Sender.selectionEnd;
                    Sender.value = Sender.value.substring(0, start) + '.' + Sender.value.substring(end, Sender.value.length);
                    Sender.selectionStart = start + 1;
                    Sender.selectionEnd   = start + 1;
                } else {
                    Sender.value += '.';
                }
                event.preventDefault();
                $(this).trigger('input');
            }
        });

        //Выделение ячеек оценок, при вводе не числового значения.
        $(document).on("input", "input.form-control", function(){
            var reg = /\d+$/;
            var value = $(this).val();
            var max = $(this).parent().parent().data('max');
            isCorrect = (value.length > 0)
                        && reg.test(value) == true
                        && value >= 0 && value <= max;

            if (!isCorrect) {
                $(this).css('border-color', 'red');
            } else {
                $(this).css('border-color', '#ccc');
            }

            var gradeGroup = $(this).parents("tr");
            //подсчет средней оценки
            calculateAvgGrade(gradeGroup);
        });

        var topicalityGrades = $("#gradeTopicality");
        var contentGrades = $("#gradeContent");
        var skillGrades = $("#gradeSkill");

        var calculateAvgGrade = function(group) {
            if (group.length > 0) {
                var grades = group.find("input:not(.active)");
                var gradesCount = grades.length;
                var gradesSum = 0;
                var notEmpty = true;
                $.each(grades, function(i,v){
                    var reg = /\d+$/;
                    var value = v.value;
                    var max = $(this).parent().parent().data('max');

                    isCorrect = (value.length > 0)
                    && reg.test(value) == true
                    && value >= 0 && value <= max;
                    if (!isCorrect) {
                        notEmpty = false;
                        group.find("input.active").val('')
                        return false;
                    }
                    gradesSum += parseFloat(v.value);
                });
            }

            if (notEmpty) {
                gradesSum = gradesSum/gradesCount;
                group.find("input.active").val(gradesSum);
            }
        };

        var calculateAllAvgGrade = function() {
            calculateAvgGrade(topicalityGrades);
            calculateAvgGrade(contentGrades);
            calculateAvgGrade(skillGrades);
        };

        var countGradeCols = function () {
            $("#gradeCols").html(topicalityGrades.find("input:not(.active)").length);
        };

        $("#moreCols").on("click", function(e){
            e.preventDefault();
            if (topicalityGrades.find('td').length >= 11) {
                return;
            }
            var td = $("<td class='table-form-control'><input type='text' class='form-control'></td>");
            topicalityGrades.find("input.active").parents("td").before(td);
            contentGrades.find("input.active").parents("td").before(td.clone());
            skillGrades.find("input.active").parents("td").before(td.clone());

            countGradeCols();
            calculateAllAvgGrade();
        });

        $("#lessCols").on("click", function(e){
            e.preventDefault();
            if (topicalityGrades.find('td').length <= 3) {
                return;
            }
            topicalityGrades.find("input.active").parents("td").prev().remove();
            contentGrades.find("input.active").parents("td").prev().remove();
            skillGrades.find("input.active").parents("td").prev().remove();

            countGradeCols();
            calculateAllAvgGrade();
        });

        //Listner клика на кнопку "Сохранить оценки"
        $('#saveCommissionGradesButton').on('click', function(e) {
            e.preventDefault();
            $(this).hide();
            var data = {
                gradeTopicality: topicalityGrades.find("input.active").val(),
                gradeContent: contentGrades.find("input.active").val(),
                gradeSkill: skillGrades.find("input.active").val()
            };
            transport.addAction(Routing.generate("diplom_ajax_save_commission_grades",{exam: __self.id}), {'ExamExpertType' : data}, $.proxy(__self.saveGradesHandler, __self));
        });

        //Listner клика на кнопку "Закончить доклад"
        $('#endReportButton').on('click', function(e) {
            e.preventDefault();
            $(this).hide();
            $('#bottomControlPanel').show();
            transport.addAction(Routing.generate("diplom_ajax_end_report",{exam: __self.id}), null, $.proxy(__self.endReportHandler, __self));
        });

        //Listner клика на кнопку "Закончить экзамен"
        $('#endExamButton').on('click', function(e){
            e.preventDefault();
            $(this).hide();
            transport.addAction(Routing.generate("diplom_ajax_exam_end",{id: __self.id}));
        });

        this.showExamControlButton();
        this.showSortReportsButton();

        if(this.currentReport || this.nextTheme){
            this.showReportControlButton(this.currentReport);
        }
        if(!this.currentReport){
            this.showTitles(this.nextTheme, this.nextStudentFio);
            this.showNextStudentTitles(this.nextNextStudentFio);
        }else{
            this.showNextStudentTitles(this.nextStudentFio);
        }
    }

    /**
     * Отобразить ФИО следующего студента
     * @param nextStudentFio
     */
    ExamForCoordinator.prototype.showNextStudentTitles = function (nextStudentFio) {
        nextStudentFio = nextStudentFio || '';
        $('#nextStudentFio').html(nextStudentFio);
    };

    ExamForCoordinator.prototype.drawReport = function(report){
        _super.prototype.drawReport(report);
        if (report.status == 'commission_grades' || report.status == 'build_protocol') {
            $('#bottomControlPanel').hide();
            this.showAddGradePanel();
            this.hideInfoPanel();

            if (report.status == 'build_protocol') {
                $('#commissionGrades').addClass('cm_form_mask');
            } else {
                $('#commissionGrades').removeClass('cm_form_mask');
            }
        }else{
            $('#bottomControlPanel').show();
        }
        this.showReportControlButton(report);
    };

    //обработка действия "Начать экзамен"
    ExamForCoordinator.prototype.startExamHandler = function(){
        this.status = 'awaiting';
        this.showExamControlButton();
        this.showReportControlButton(this.currentReport);
    };

    ExamForCoordinator.prototype.saveGradesHandler = function(data) {
        if (data.errors !== null && typeof data.errors === 'object') {
            $('.error').remove();
            $.each(data.errors, function(k, v){
                var td = $("#"+ k).find("td");
                td.eq(0).append("<span class='error'>" + v + "</span>");
            });

            $('#saveCommissionGradesButton').show();
            return false;
        }

        this.currentReport.status = 'build_protocol';
        this.drawReport(this.currentReport);
    };

    //обработка действия "Закончить доклад"
    ExamForCoordinator.prototype.endReportHandler = function(data){
        $('#endExamButtonContainer').show();
        this.status = 'awaiting';
        this.currentReport = null;

        //отобразить следующих студентов
        this.showTitles(data.data.nextTheme, data.data.nextStudentFio);
        this.showNextStudentTitles(data.data.nextNextStudentFio);
        //убираем панель оценок
        this.hideAddGradePanel();
        this.showInfoPanel();

        if(data.data.nextTheme){
            this.showReportControlButton();
        }else{
            this.hideReportControlButton();
        }
    };

    /**
     * Отобразить кнопки управления отчетом
     */
    ExamForCoordinator.prototype.showReportControlButton = function(report){
        this.hideReportControlButton();
        var activeStatuses = ['awaiting', 'speech'];

        if(activeStatuses.indexOf(this.status) == -1){
            return false;
        }

        if (report == null) {
            $('#startReportButton').show();
        } else if (report.status == 'speech') {
            $('#reportToDiscussionButton').show();
        } else if (report.status == 'discussion') {
            $('#reportToProtocolButton').show();
        } else if (report.status == 'commission_grades') {
            $('#saveCommissionGradesButton').show();
        } else if (report.status == 'build_protocol') {
            $('#protocolButton').prop('href', Routing.generate("diplom_coordinator_report_protocol", {id: this.currentReport.id})).show();
        }
    };

    ExamForCoordinator.prototype.hideInfoPanel = function(){
        $("#infoPanel").hide();
    };

    ExamForCoordinator.prototype.showInfoPanel = function(){
        $("#infoPanel").show();
    };

    /**
     * Отобразить панель оценок
     */
    ExamForCoordinator.prototype.showAddGradePanel = function() {
        $("#commissionGrades").show();
    };
    /**
     * Скрыть панель оценок
     */
    ExamForCoordinator.prototype.hideAddGradePanel = function() {
        var $commissionGrades = $("#commissionGrades");
        $commissionGrades.find('input[type="text"]').val('');
        $commissionGrades.hide();
    };
    /**
     * Скрыть кнопки управления отчетом
     */
    ExamForCoordinator.prototype.hideReportControlButton = function(){
        $('.reportControlButton').hide();
    };

    /**
     * Отобразить кнопку редактирования порядка докладов
     */
    ExamForCoordinator.prototype.showSortReportsButton = function(){
        //отобразить кнопку редактировать порядок докладов
        $('#sortReportsButtonContainer').show();
    };

    ExamForCoordinator.prototype.showExamControlButton = function(){
        //отобразить кнопку начать/закончить экзамен
        if(this.status == 'preparation'){
            $('#startExamButtonContainer').show();
            $('#endExamButtonContainer').hide();
        }else if(this.status == 'awaiting'){
            $('#endExamButtonContainer').show();
            $('#startExamButtonContainer').hide();
        }else
        {
            $('#endExamButtonContainer').hide();
            $('#startExamButtonContainer').hide();
        }
    };

    ExamForCoordinator.prototype.openReportSortModal = function(){
        $('#modal').modal().open();
        var transport = new DesktopTransport();
        transport.addAction(Routing.generate("diplom_ajax_get_awaiting_reports"), null, loadReportsCallback);

        function loadReportsCallback(data) {
            var reports = data.data.reports;
            var reportObj = $("#sortable");
            reportObj.empty();
            if (reports.length > 0) {
                $.each(reports, function(i,v){
                    var row = '<div data-id="' + v.id + '" class="cm_listdrag-item"><div class="cm_listdrag-item-title">' + v.student + '</div></div>'
                    reportObj.append(row);
                })
            }
            initReportSortable();
        }
    };

    /**
     * Сортировка drag'n'drop
     */
    var initReportSortable = function(){
        var sortableList = $( "#sortable" );
        var startOrdies = [];
        var resultOrdies = [];
        sortableList.sortable({
            start: function(event, ui) {
                var count = $(".ui-sortable-handle").length;

                for ( var i = 0; i < count; i++ ) {
                    startOrdies.push(i);
                }
            },
            update: function(event, ui) {
                var newIndex = ui.item.index();
                var oldIndex = arraySearch(startOrdies, newIndex);

                resultOrdies = startOrdies.slice(0);
                resultOrdies.move(oldIndex, newIndex);

                $(".ui-sortable-handle").each(function(){
                    $(this).find("td:eq(0)").html(startOrdies[$(this).index() + 1]);
                });
                transport.addAction(Routing.generate("diplom_screen_ajax_report_change_position", {id: ui.item.data("id")} ), { position: newIndex }, function(data){
                    $('#reportTitle').html(data.data.theme);
                    $('#studentFio').html(data.data.fio);
                    $('#nextStudentFio').html(data.data.nextStudentFio)
                });

            }
        });
    };

    Array.prototype.move = function (old_index, new_index) {
        if (new_index >= this.length) {
            var k = new_index - this.length;
            while ((k--) + 1) {
                this.push(undefined);
            }
        }
        this.splice(new_index, 0, this.splice(old_index, 1)[0]);
        return this; // for testing purposes
    };

    var arraySearch = function(arr,val) {
        for (var i=0; i<arr.length; i++)
            if (arr[i] === val)
                return i;
        return false;
    };

    ExamForCoordinator.prototype.reportToComplete = function() {
        _super.prototype.reportToComplete();
        this.showExamControlButton();
    };

    return ExamForCoordinator;
})(ExamForChairman);

var Report = (function () {
    function Report(options) {
        this.id = options.id;
        this.name = options.name;
        this.status = options.status;
        this.questions = options.questions;
        this.organizationId = options.organizationId;
        this.studentFio = options.studentFio;
    }

    return Report;
})();
