var ExamClient = null;

(function() {
    ExamClient = function(exam, session, topics, invariableTopics) {
        var refreshPage = function(topic, event) {
            window.location.reload();
        };

        var startReport = function(topic, event) {
            unSubscribeAll();
            subscribe(event.data.topics);
            exam.reportNew(event.data.report);
        };

        var switchReportStatus = function(topic, event) {

            switch (event.data.status) {
                case 'discussion':
                    exam.reportToDiscussion();
                    break;
                case 'build_protocol':
                    exam.reportToProtocol();
                    break;
                case 'completed':
                    exam.reportToComplete();
                    break;
            }
        };

        var newQuestion = function(topic, event) {
            exam.questionNew(event.data);
        };

        var newGrade = function(topic, event) {
            exam.gradeNew(event.data);
        };

        var newScreenShot = function(topic, event) {
            exam.screenShotNew();
        };

        // Карта соответствия команд, приходящих с сервера с их js колбэками
        var map = {
            waitExamPreparation: refreshPage,
            waitExamStart: refreshPage,
            switchExamReport: startReport,
            switchReportStatus: switchReportStatus,
            getQuestionsByReport: newQuestion,
            getReportGrades: newGrade,
            getReportScreenShots: newScreenShot,
            waitExamEnd: refreshPage
        };

        // Текущие подписки
        var currentSubscribes = {};
        var subscribe = function(topics) {
            for (var topic in topics) {
                if (topics.hasOwnProperty(topic)) {
                    currentSubscribes[topic] = topics[topic];
                    session.subscribe(topic, function(t, e) { map[currentSubscribes[t]](t, e); });
                }
            }
        };

        var unSubscribeAll = function() {
            for (var topic in currentSubscribes) {
                if (currentSubscribes.hasOwnProperty(topic)) {
                    session.unsubscribe(topic);
                }
            }
            currentSubscribes = {};
        };

        // Текущие подписки на постоянные топики (Подписка не слетает при вызове unSubscribeAll)
        var invariableSubscribes = {};
        var subscribeInvariableTopics = function() {
            for (var topic in invariableTopics) {
                if (invariableTopics.hasOwnProperty(topic)) {
                    invariableSubscribes[topic] = invariableTopics[topic];
                    session.subscribe(topic, function(t, e) { map[invariableSubscribes[t]](t, e); });
                }
            }
        };

        if (invariableTopics !== undefined) {
            subscribeInvariableTopics();
        }
        subscribe(topics);
    };
})();
