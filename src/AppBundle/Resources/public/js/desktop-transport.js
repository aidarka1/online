var DesktopTransport = null;
(function($) {
    /**
     * Взято с проекта очереди
     * Класс для работы с удалённым сервером.
     * Реализует remote-методы для обмена клиентского интерфейса данными с сервером.
     * Сколько бы экземпляров данного класса не было создано, все они
     * будут использовать одну очередь запросов к серверу.
     */
    DesktopTransport = function() {

        // Статические переменные для поддержки единой очереди для
        // всех экземпляров транспорта
        // Очередь запросов
        DesktopTransport.ajaxQueue			= [];
        // Индикатор процесса обмена с сервером
        DesktopTransport.isSending			= false;

        // Отправка всех заданий очереди на сервер
        // по одному, чтобы не грузить сервер.
        function sendAjaxQueue() {

            // Если в очереди нет заданий, то нечего и отправлять
            if (DesktopTransport.ajaxQueue.length > 0) {

                // Начали отправку данных
                DesktopTransport.isSending = true;
                // Слудующее задание на отправку
                var nextAjaxTask = DesktopTransport.ajaxQueue.shift();

                // Отправим задание на сервер
                $.ajax({
                    url: nextAjaxTask.url,
                    type: "POST",
                    dataType: "json",
                    data: nextAjaxTask.params,
                    async: nextAjaxTask.async,
                    beforeSend: function() {
                        if (nextAjaxTask.loaderContainer instanceof jQuery) {
                            nextAjaxTask.loaderContainer.find(".loading-mask").show();
                        }
                    },
                    success: function(data) {
                        if (data.status == 'ok') {
                            // Передадим пользовательскому обработчику полученный ответ
                            if (typeof nextAjaxTask.callback === "function") {
                                nextAjaxTask.callback(data);
                            }
                            sendAjaxQueue();
                        } else {
                            clearQueue();
                            if (data.message == 'login') {
                                // Перезагрузка страницы происходит не сразу, пока развлекаемся крутяшкой
                                window.location.reload(true);
                            } else {
                                showErrorMessage(data.message);
                            }
                        }
                    },
                    error: function(xhr, status, message) {
                        clearQueue();
                        if (message) {
                            showErrorMessage(message);
                        }
                    },
                    complete: function() {
                        if (nextAjaxTask.loaderContainer instanceof jQuery) {
                            nextAjaxTask.loaderContainer.find(".loading-mask").hide();
                        }
                    }
                });

            } else {
                // Закончили отправку данных
                DesktopTransport.isSending = false;
            }

        }

        function clearQueue() {
            DesktopTransport.ajaxQueue = [];
            DesktopTransport.isSending = false;
        }

        function showErrorMessage(message, loaderContainer) {
            alert(message);
        }

        // Конструктор

        function __construct() {

            /**
             * Добавляет запрос к очереди на отправку.
             * Запрос отправляется как только будут отправлены все
             * запросы, стоящие в очереди перед ним.
             * @param remoteAction
             * @param requestParams
             * @param callback
             * @param loadingContainer Элемент, над которым будет отображаться индикатор аякс загрузки, либо null
             * @param async По умолчанию true
             */
            this.addAction = function(remoteAction, requestParams, callback, loadingContainer, async) {

                if (async === undefined) {
                    async = true;
                }

                // Добавим запрос в очередь
                DesktopTransport.ajaxQueue.push({
                    url:	            remoteAction,
                    params:		        requestParams,
                    callback:           callback,
                    loaderContainer:    loadingContainer,
                    async:              async
                });

                // Инициализируем отправку из очереди если необходимо
                if (!DesktopTransport.isSending) {
                    sendAjaxQueue();
                }
            }

        }

        return new __construct();
    };

})(jQuery);